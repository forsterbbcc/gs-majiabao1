### 目录
```
.
├── README.md                   // help
├── build                       // 打包输出目录
│   └──  demo                   // Demo项目生产环境输出
├── config                      // webpack配置目录
├── public                      // 启动页目录 
├── src                         // 代码根目录
│   ├── core                    // 框架核心库
│        ├──  navigation        // 导航模块
│        └──  stage             // 舞台模块     
│   ├── assets                  // 公共图片资源
│   ├── components              // UI组件目录
│        ├──  common            // 公共组件目录
│        └──  emulate           // 仿真组件目录
│   ├── lib                     // 公共库
│   ├── pro                     // 业务库
│        ├──  event             // 业务事件目录
│        └──  module            // 业务模块目录
│   └── project                 // 项目代码库
│        └──  demo              // Demo项目
│              ├──  assets      // 项目图片资源目录
│              ├──  components  // 项目UI组件目录
│              ├──  style       // 项目样式目录
│              ├──  view        // 项目页面目录
│              ├──  App.js      // 项目入口文件
│              └──  tradeIndex.js    // 项目启动文件
│
├── document                    // 框架规范文档
├── node_modules                // 项目依赖包
└── package.json                // 项目配置信息


│
├──
└──
```