## Javascript语法规范

### 常量与变量

#### 何时使用常量
- 参数是对象,数组,方法时使用常量   
- 参数时固定的数字或字符串时使用常量
- 参数在使用过程中不会使用 **=** 寄存器重新赋值

#### 何时应该使用常量
没有任何说明的参数,应该使用语意化的常量命名,使内容一目了然
```javascript
if (value.length < 8) { // 为什么要小于8，8表示啥？长度，还是位移，还是高度？Oh,my 
	
}
const MAX_INPUT_LENGTH = 8;
if (value.length < MAX_INPUT_LENGTH) { // 一目了然，不能超过最大输入长度
	
}
```
 
#### 何时使用变量
- 参数是数字或字符串,且参数会发生变化时
- 参数在使用过程中会被 **=** 寄存器重新赋值
- 通常不推荐对象,数组,方法重新赋值
 
#### 何时不应该使用变量
没有使用过的变量要删除掉,以后还会用的要注释掉并写明原因   
变量只使用一次的无需使用变量
```javascript
let kpi = 4;  // 没用的就删除掉，不然过三个月自己都不敢删，怕是不是那用到了
function example() {
	let a = 1;
	let b = 2;
	return 2a+b+1;
}
```
 
### 命名规则
**预设常量**(字符串,数字),使用全大写命名,长单词使用下划线拼接
```javascript
const APP = 1;
const START_DATE = '2018';
```
**引入式常量**通常不另设命名,如需另设命名,需遵守小驼峰命名法
```javascript
const {data} = result
```
**对象**、**数组**、**函数**使用常量时，遵守小驼峰命名法
```javascript
const myName = {}
```
**构造函数**使用大驼峰命名法
```javascript
const Worker = function(){
 
};
 
function Worker (){
   
};
```
**变量**使用小驼峰命名法
```javascript
let myName;
let age = 1;
```


### 判断的使用
任何时候都使用绝对等于 **===**
```javascript
if(0 === true){}
```
当判断参数存在时,使用隐式判断
```javascript
let a = 1;
if(!!a){}
```
当判断参数不为0,空字符串,`null`,`undefined`时,使用隐式判断
```javascript
let a;
if(!a){}
```


### Render中的JS

参考如下代码,当Component中需要使用超过一次的判断的时,使用单独的方法来进行判断和返回需渲染的模块,来提高代码的可读性
```javascript
import React,{Component} from 'react';

class App extends Component{
    constructor(props){
        super(props)
        this.state = {
            swift:true,
            pay:true
        }
    }
    
    render(){
        return(
            <div>
                <div>
                    {
                        this.state.swift?(this.stat.pay?<div>3</div>:<div>4</div>):<div>2</div>
                    }
                    <this.test />
                </div>
                <div>
                {
                    this.state.switch?<div>1</div>:<div>2</div>  
                }
                </div>
            </div>
        )
    }
    
    /**
    * @method
    * @description 描述
    */
    test(){
        const {swift,pay} = this.state;
        if(swift){
            if(pay){
                return <div>3</div>
            }else{
                return <div>4</div>
            }
        }else{
            return <div>2</div>
        }
    }
}
```

#### Component中的方法传递
给原生Component绑定事件,或向自定义Component传递事件,可参考如下代码,使用**点击3**的方法来传递
```javascript
import React,{Component} from 'react';

class App extends Component{
    constructor(props){
        super(props);
        this.state = {
            attr:'bind'
        }
    }
    
    render(){
        return(
            <div>
                <div onClick={()=>{
                    this.broadcast(this.state.bind);
                }}>点击1</div>
                <div onClick={this.broadcast.bind(this,this.state.bind)}>点击2</div>
                <div onClick={this.broadcast2} data-attr={this.state.bind}>点击3</div>
            </div>
        )
    }
    
    /**
    * @method
    * @description 描述
    */
    broadcast(attr){
        console.log(attr);
    }
    
    broadcast2=(e)=>{
        const attr = e.target.getAttribute('data-attr');
        console.log(attr);
    }
}
```