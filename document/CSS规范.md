## CSS规范


### 样式的层级
常见的dom层级关系如下
```html
<div>
    <p>文字</p>
</div>
```
应用写法如下
```scss
div{
  p{
    font-size: 16px;  
  }
}
```
在实际项目中,以上写法经常构成样式污染,因此我们使用以下写法:
```html
<div class="top">
    <p class="content">文字</p>
</div>
```
为每一个dom赋予一个**class**
```scss
.top{
  .content{
    font-size: 16px;  
  }
}
```
### 样式的命名规范
css样式使用全小写命名,样式尽量使用单词构成 `.home`   
复合单词使用 **-** 连接,如
```scss
.board-title{}
```
框架的公共样式class使用 **common** 以及组件名组成 使用 **-** 连接
```html
<input type="text" class="common-input">
```
每一个stage(页面),顶层必须定义一个class,格式为
```html
<div class="view-home"></div>
```
由 **view** 以及页面的导航名组成,使用 **-** 连接

### 何时以及如何使用nth以及eq样式
当需要定位到一个列表级dom中的首个,最后一个,以及复合规律的个体时(如odd),我们使用nth或eq
```html
<ul class="list">
    <li>1</li>
    <li>2</li>
    <li>3</li>
</ul>
```
```scss
.list{
    li:first-child{
    
    }
    li:last-child{
    
    }
    li:nth-child(2n){
    
    }
}
```