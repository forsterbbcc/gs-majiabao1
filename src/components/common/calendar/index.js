import React,{Component} from 'react'
import './index.scss'


class Calendar extends Component {
    constructor(props) {
        super(props)
        this.state = {
            year: null,//当前年份
            month: null,//当前月份
            day: null,//当前日期
            activeDay: null,//选择的日期
            dayList: [  //当前渲染的日期范围
                {
                    day: undefined,
                    week: "日"
                },
                {
                    day: undefined,
                    week: "一"
                },
                {
                    day: undefined,
                    week: "二"
                },
                {
                    day: undefined,
                    week: "三"
                },
                {
                    day: undefined,
                    week: "四"
                },
                {
                    day: undefined,
                    week: "五"
                },
                {
                    day: undefined,
                    week: "六"
                },
            ]
        }
    }

    /**
     * 解析当前日期显示范围
     */
    componentWillMount() {
        const now = new Date()
        const year = now.getFullYear()
        const month = now.getMonth() + 1;
        const day = now.getDate()
        const week = now.getDay()
        //本月最大天数
        const this_month_maxDay = new Date(year, month, 0).getDate()
        //上月最大天数
        const last__month_maxDay = new Date(year, month - 1, 0).getDate()
        let first_date = new Date(year, month - 1, day - week).getDate();
        let dateList = this.state.dayList;

        dateList.map((date, index) => {
            //判断左侧日期是否小于 1 号
            if (day - week + index <= 0) {
                date.day = first_date + index
            } else {
                //判断右侧日期是否大于本月最大日期
                if (day + index - week > this_month_maxDay) {
                    date.day = (day + index - week) - this_month_maxDay
                } else {
                    date.day = day + index - week
                }
            }

        })
        this.setState({
            year,
            month: month < 10 ? "0" + month : month,
            day,
            activeDay: day,
            dayList: dateList
        })
    }

    /**
     * 选择日期事件
     */
    onSetActiveDay(date) {
        const { onChange } = this.props;
        onChange && onChange(date.day)
        this.setState({
            activeDay: date.day
        })
    }

    renderDay = () => {
        const { dayList, activeDay } = this.state;
        return dayList.map((date, index) => {
            return (
                <div className="date" key={index}>
                    <span className='week color-gray'>{date.week}</span>
                    <div
                        className={activeDay == date.day ? "active day" : "day"}
                        onClick={this.onSetActiveDay.bind(this, date)}
                    >{date.day}</div>
                </div>
            )
        })

    }


    render() {
        const { year, month } = this.state;
        const {style} = this.props
        return (
            <div style={style} className="c-calendar">
                <div className="title">{year}年{month}月</div>
                <div className="content flex-between-all">
                    {this.renderDay()}
                </div>
            </div>
        )
    }
}

export default Calendar;
