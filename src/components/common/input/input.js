import React, {Component} from 'react';
// import './input.scss'
import {toPassword} from "../../lib/utils";

class Captcha extends Component {

    constructor(props) {
        super(props);
        this.state = {
            coldDown: this.props.captchaColdDown,
        }
    }

    render() {
        const {captchaStyle} = this.props;
        return (
            <div
                className={`element-button ${captchaStyle} ${this.state.coldDown !== this.props.captchaColdDown ? 'disable' : ''} ${this.state.coldDown !== this.props.captchaColdDown ? this.props.captchaDisableStyle : ''}`}
                onTouchEnd={this.action}>{this.textOutput()}</div>
        )
    }

    textOutput() {
        if (this.state.coldDown !== this.props.captchaColdDown) {
            return this.props.captchaReplace.replace('%s', this.props.captchaColdDown - this.state.coldDown)
        } else {
            return this.props.captcha
        }
    }

    action = () => {
        if (this.state.coldDown === this.props.captchaColdDown) {
            const result = this.props.captchaAction();
            if (result) {
                this.setState({coldDown: 0}, () => {
                    this.countdown();
                });
            }
        }
    };

    countdown() {
        if (this.state.coldDown !== this.props.captchaColdDown) {
            this.setState({coldDown: this.state.coldDown + 1});
            setTimeout(() => this.countdown(), 1000)
        }
    }
}

export class EInput extends Component {
    instance = null;
    config = null;

    constructor(props) {
        super(props);


        this.state = {
            backup: '',
            value: '',
            focus: false,
            placeholderDisplay: true,
            hide: true,
        };
    }

    render() {
        this.config = Object.assign({
            type: 'text',
            wrapperStyle: '',
            inputStyle: '',
            borderStyle: '',
            placeholderStyle: '',
            captchaStyle: '',
            captchaDisableStyle: '',
            placeholder: '',
            maxLength: 100,
            fontSize: 14,
            placeholderSize: 16,
            hasEye: false,
            eye: [],
            captcha: '',
            captchaReplace: '',
            captchaColdDown: 60,
            num: false,
            imgCode: false,
            imgAction(){
                console.warn('尚未绑定图片点击事件')
            },
            captchaAction() {
            },
            value() {
            }
        }, this.props);

        if (typeof this.config.value !== 'function') {
            console.error('value 值方法必须是Function')
        }
        const {focus, hide, placeholderDisplay, backup, value} = this.state;
        const {borderStyle, wrapperStyle, inputStyle, placeholderStyle, fontSize, placeholderSize, icon, placeholder, type, captcha, hasEye, eye, num, imgCode} = this.config;
        return (
            <div className={`E-input element-container ${wrapperStyle}`}
                 style={{fontSize: fontSize}}>
                {
                    focus ? <div className={`element-border ${borderStyle}`}/> : null
                }
                {
                    !num ? (
                        <div className={'element-iconBox small'}>
                            <img src={icon} alt=""/>
                        </div>
                    ) : (
                        <div className={'element-iconBox small'}>
                            <div className={'num'}>+86</div>
                        </div>
                    )
                }
                <div className={'element-inputBox'}>
                    <div
                        className={`element-substitute ${inputStyle}`}>{hide ? value : backup}</div>
                    {
                        placeholderDisplay ? <div
                            className={`element-placeholder ${placeholderStyle}`}
                            style={{fontSize: placeholderSize}}>{placeholder}</div> : null
                    }
                    <input type={type} ref={(e) => {
                        this.instance = e
                    }} onInput={this.input} onFocus={this.focusSubstitute} onBlur={this.blurSubstitute}
                           maxLength={this.config.maxLength}/>
                </div>
                {
                    hasEye ? (
                        <div className={'element-iconBox small'}
                             onTouchEnd={() => this.setState({hide: !hide})}>
                            <img src={eye[hide ? 0 : 1]} alt=""/>
                        </div>
                    ) : null
                }
                {
                    imgCode ? (
                        <div className={'element-imgBox'}>
                            <img src={this.props.code} alt="" onTouchEnd={this.config.imgAction}/>
                        </div>
                    ) : null
                }
                {
                    captcha !== '' ? (<Captcha {...this.config}/>) : null
                }
            </div>
        )
    }

    /**
     * 输入回调
     * @param e
     */
    input = (e) => {
        let v, c;
        v = c = e.currentTarget.value;
        // console.log(v);
        // console.log(e.currentTarget.validity);
        if (v === '' && this.state.value.length === 1 && e.currentTarget.validity.valid) {
            this.setState({value: v, backup: c}, () => {
                this.config.value(c)
            })
        } else if (v !== '') {
            //增加text输入中文的特殊处理 input=text时 额外判断valid
            if (this.countLength(v)) {
                if (this.config.type === 'password') v = toPassword(v);
                this.setState({value: v, backup: c}, () => {
                    this.config.value(c)
                })
            } else {
                e.currentTarget.value = this.state.backup
            }
            this.setState({placeholderDisplay: false})
        } else {
            this.instance.value = this.state.value
        }

    };

    /**
     * 焦点回调
     */
    focusSubstitute = () => {
        this.setState({focus: true, placeholderDisplay: false})
    };

    /**
     * 失焦回调
     */
    blurSubstitute = () => {
        if (this.state.value === '') {
            this.setState({placeholderDisplay: true})
        }
        this.setState({focus: false})
    };

    countLength(val) {
        return val.length <= this.config.maxLength
    }
}