import React , {Component} from 'react'
import { ListView, PullToRefresh } from 'antd-mobile';

export default class PullList extends Component {

    constructor(props){
        super(props);

        const dataSource = new ListView.DataSource({
            rowHasChanged: (row1, row2) => row1 !== row2,
        });

        this.state = {
            dataSource,
            isLoading:false,
            refreshing: false, 
            isRefreshLoading: false,
            hasMore:true
        }
    }

    componentWillReceiveProps(nextProps){
        this.setState({dataSource:this.state.dataSource.cloneWithRows(nextProps.data)});
    }

    render(){
        /** 根据 onRefresh 返回有无下拉刷新 */
        let refresh = !!this.props.onRefresh ? <PullToRefresh refreshing={this.state.refreshing} onRefresh={this.onRefresh}/> : ''
        /** 判断 最终上拉距离是使用默认的20还是自定义的距离 */
        let endReachedThreshold = !!this.props.onLoadMore && this.props.endReachedThreshold === 0 ? 20 : this.props.endReachedThreshold;
        /** 如果存在上拉函数 把上拉距离最终确定 */
        let finalEndReachedThreshold = !!this.props.onLoadMore ? endReachedThreshold : 0

        return(
            <ListView dataSource={this.state.dataSource} renderRow={this.props.renderRow} style={this.props.style} prefixCls={this.props.prefixCls} onEndReached={this.onEndReached} onEndReachedThreshold={finalEndReachedThreshold} pullToRefresh={refresh}/>
        );
    }

    onEndReached = async(event) => {
        
        if (this.state.isLoading ) {
            return false;
        }
        if ( !this.state.hasMore) {
            return false;
        }
        this.setState({ isLoading: true });

        try {
            await this.props.onLoadMore && this.props.onLoadMore();
        } catch (error) {
            console.warn(error);
        }
        this.setState({ isLoading: false });
    }

    onRefresh = async() => {
        this.setState({ refreshing: true, isRefreshLoading: true });

        try {
            await this.props.onRefresh();
        } catch (error) {
            console.warn(error);
        }
        this.setState({ refreshing: false, isRefreshLoading: false });
    };
}