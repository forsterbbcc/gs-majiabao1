import React, {Component} from 'react';
import ReactSvg from 'react-svg';
import "./svg.scss"

export class CSvg extends Component {
    render() {
        return (
            <ReactSvg src={this.props.src} svgClassName={this.props.svgClassName} className={`C-Svg ${this.props.className}`}/>
        )
    }
}