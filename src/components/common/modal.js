import ReactDOM from "react-dom";
import React, {Component} from "react";
import './modal.scss';

export class CModal extends Component {

    holder = null;
    create = null;

    constructor(props) {
        super(props);
    }

    render() {
        const {visible, background = 'black'} = this.props;
        if (visible && !this.holder) {
            this.holder = document.createElement('div');
            if (['white', 'black'].includes(background)) {
                this.holder.className = `C-modal ${background}`;
            } else {
                this.holder.className = 'C-modal';
                this.holder.style.background = background;
            }

            document.body.appendChild(this.holder);
        } else if (!visible) {
            if (this.holder) {
                document.body.removeChild(this.holder);
                this.holder = null;
                this.create = null;
            }
        }
        return null
    }

    componentDidUpdate() {
        if (this.holder && !this.create) {
            const {onCancel = this.close} = this.props;
            this.create = true;
            ReactDOM.render(<Instance onCancel={onCancel} {...this.props} />, this.holder);
        }
    }

    close = () => {
        document.body.removeChild(this.holder);
        this.holder = null;
        this.create = null;
    }
}

class Instance extends Component {
    render() {
        return (
            <div>
                <div className={'element-bg'} onTouchEnd={this.props.onCancel}/>
                <div className={'element-body'}>{this.props.children}</div>
            </div>
        )

    }
}