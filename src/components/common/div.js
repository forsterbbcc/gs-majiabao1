import React, {Component} from 'react';
import {EVENT} from "../../pro/event";
import {withNavigation} from "../../core/navigation";

export class App extends Component {
    start = 0;
    end = 0;
    gap = 20;

    longTap = true;

    render() {
        const {text=null,children,className,activeClassName,active} = this.props;
        if(text){
            return (
                <span onTouchStart={this.touchStart} onTouchMove={this.touchMove} onTouchEnd={this.touchEnd}
                     className={`a ${className} ${active?activeClassName:''}`}>
                    {children}
                </span>
            )
        }else{
            return (
                <div onTouchStart={this.touchStart} onTouchMove={this.touchMove} onTouchEnd={this.touchEnd}
                     className={`a ${className} ${active?activeClassName:''}`}>
                    {children}
                </div>
            )
        }

    }

    touchStart = () => {
        this.start = Date.now();
    };

    touchMove = () => {
        this.longTap = false;
    };

    touchEnd = () => {
        this.end = Date.now();
        if (this.end - this.start < this.gap || this.longTap === true) {
            const {event, onClick, onTouchEnd, goTo, tab, state = {}, auth = false, login = 'login'} = this.props;
            if (!!event) return event();
            if (!!onClick) return onClick();
            if (!!onTouchEnd) return onTouchEnd();
            if (!!goTo) {
                if (auth) {
                    if (!EVENT.Account.isLogin) return this.props.navigate.goTo(login);
                }
                return this.props.navigate.goTo(goTo, state);
            }
            if (!!tab) {
                return this.props.navigate.tab(tab, state)
            }
        }
        this.longTap = true;
    };

}

export const Div = withNavigation(App);