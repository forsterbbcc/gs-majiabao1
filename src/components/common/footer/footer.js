import React, {Component} from 'react'
import {CSvg} from '../svg';
import './footer.scss'

export default class Footer extends Component {

    render() {
        return (
            <div className={`footer ${this.props.className}`}>
                {this.renderView()}
            </div>
        );
    }

    renderView() {
        const {show, goTo, source} = this.props;
        return Object.keys(source).map((key, index) => {
            const {icon, activeIcon = icon, textLabel = ''} = source[key];
            return (
                <div onClick={() => goTo(key)} key={index}>
                    <div
                        className={`foo-imageBox ${show === key ? 'foo-active-image' : 'foo-default-image'}`}>
                        <CSvg src={show === key ? activeIcon : icon} svgClassName={'foo-image'}  className={`${show===key?'active':''}`}/>
                    </div>
                    <div className={`foo-textLabel ${show === key ? 'foo-active-textLabel' : 'foo-default-textLabel'}`}>{textLabel}</div>
                </div>
            );
        });
    }
}