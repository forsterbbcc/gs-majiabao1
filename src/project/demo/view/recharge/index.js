import React from 'react';
import {createStage} from "../../../../core/stage";
import {EVENT} from "../../../../pro/event";
import {Actor} from "../../../../core/stage/actor";
import {Header} from "../component/ui";
import "./index.scss"
import {CSvg} from "../../../../components/common/svg";


import alipay from '../../assets/svg/payment/alipay.svg';

class App extends Actor {
    constructor(props) {
        super(props);
        this.state = {
            list: []
        }
    }

    render() {
        console.log(this.props.navigation);
        return (
            <div className={'stage-recharge'}>
                <Header title={'账户充值'} {...this.props}/>
                <div onClick={()=>this.props.navigate.goBack()}>Back</div>
                <div className={'payment-list'}>
                    {
                        this.state.list.map(([url, {name, subtitle}], key) => {
                            return (
                                <div className={'role'} key={key} onTouchEnd={()=>{this.props.navigate.goTo('xxpay')}}>
                                    <div className={'icon'}>
                                        <CSvg src={alipay} svgClassName={'logo'}/>
                                    </div>
                                    <div className={'body'}>
                                        <span className={'title'}>{name}</span>
                                        <span className={'subtitle'}>{subtitle}</span>
                                    </div>
                                </div>
                            )
                        })
                    }
                </div>
            </div>
        )
    }

    async componentDidMount() {
        try {
            console.log(EVENT.Recharge.root);
            await EVENT.Account.submit('15112345666', '123qwe');
            EVENT.Account.callback();
            await EVENT.Recharge.updatePayment();
            this.setState({list: Object.entries(EVENT.Recharge.config)})
        } catch (err) {
            EVENT.Error.throw(err)
        }
    }

    componentWillUnmount() {

    }

}

export default createStage(App);