import React, {Component} from 'react';
import {createStage} from "../../../../core/stage";
import './xxpay.scss';
import {Header,Button} from "../component/ui";

class App extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div className={'stage-xxpay'}>
                <Header title={'xxpay'}/>
                充值页面
                <Button event={()=>{this.props.navigate.goBack()}}>返回</Button>
            </div>
        )
    }

    componentDidMount() {

    }

    componentWillUnmount() {

    }
}

export default createStage(App)