import React, {Component} from 'react';
import {Actor} from "../../../../core/stage/actor";
import {createStage} from "../../../../core/stage";
import './register.scss';
import {EInput} from "../../../../components/emulate/input";
import {Button} from "../component/ui";
import {CModal} from "../../../../components/common/modal";
import title from '../../assets/svg/title.svg';
import phone from '../../assets/svg/phone.svg';
import lock from '../../assets/svg/lock.svg';
import {EVENT} from "../../../../pro/event";


class App extends Actor {
    constructor(props) {
        super(props);
        this.state = {
            visible: false,
            stage1:'',
            stage2:'',
            text:'下一步'
        }
    }

    render() {
        const {visible} = this.state;
        return (
            <div className={'stage-register'}>
                <img src={title} alt={'title'} className={'title'}/>
                <div className={'swipe'}>
                    <div className={`window ${this.state.stage1}`}>
                        <EInput
                            icon={phone}
                            type={'tel'}
                            placeholderStyle={'placeholder'}
                            inputStyle={'input'}
                            placeholder={'请输入手机号'}
                            borderStyle={'border'}
                            wrapperStyle={'wrapper'}
                            value={this.setName}
                            maxLength={11}
                            fontSize={12}
                            placeholderSize={14}
                        />
                        <EInput
                            icon={lock}
                            type={'tel'}
                            placeholderStyle={'placeholder'}
                            inputStyle={'input'}
                            placeholder={'请输入短信验证码'}
                            borderStyle={'border'}
                            wrapperStyle={'wrapper'}
                            value={this.setName}
                            maxLength={4}
                            fontSize={12}
                            placeholderSize={14}
                            captcha={'获取验证码'}
                            captchaReplace={'%s秒后重试'}
                            captchaAction={this.getCaptcha}
                            captchaStyle={'captcha'}
                        />
                    </div>
                    <div className={`window ${this.state.stage2}`}>
                        <EInput
                            icon={phone}
                            type={'text'}
                            placeholderStyle={'placeholder'}
                            inputStyle={'input'}
                            placeholder={'请输入昵称'}
                            borderStyle={'border'}
                            wrapperStyle={'wrapper'}
                            value={this.setName}
                            maxLength={18}
                            fontSize={12}
                            placeholderSize={14}
                        />
                        <EInput
                            icon={lock}
                            type={'password'}
                            placeholderStyle={'placeholder'}
                            inputStyle={'input'}
                            placeholder={'请输入密码'}
                            borderStyle={'border'}
                            wrapperStyle={'wrapper'}
                            value={this.setName}
                            maxLength={18}
                            fontSize={12}
                            placeholderSize={14}
                        />
                    </div>
                </div>
                <Button event={this.next}>{this.state.text}</Button>
                <CModal visible={visible} onCancel={() => this.setState({visible: false})}>
                    <div className={'stage-register-panel'}>
                        <div className={'title'}>验证提示</div>
                        <div className={'body'}>显示个验证码图片先</div>
                        <div className={'bar'}>
                            <Button className={'button'}>确定</Button>
                            <div className={'divide'}/>
                            <Button className={'button'} event={() => this.setState({visible: false})}>取消</Button>
                        </div>
                    </div>
                </CModal>
            </div>
        )
    }

    componentDidMount() {

    }

    componentWillUnmount() {

    }

    setName = (e) => {
        console.log(e)
    };

    getCaptcha = () => {
        this.setState({visible:true})
    };

    next=()=>{
        if(this.state.stage1 === ''){
            this.setState({
                stage1:'_move-out',
                stage2:'_move-in',
                text:'上一步'
            })
        }else{
            this.setState({
                stage1:'',
                stage2:'',
                text:'下一步'
            })
        }
    };
}

export default createStage(App);