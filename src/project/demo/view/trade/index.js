import React, {Component} from 'react';
import {createStage} from "../../../../core/stage/index";
import "./index.scss";
import {Chart} from "../../../../pro/chartTV/chart";
import {Actor} from "../../../../core/stage/actor";

class App extends Actor {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div className="Trade">
                交易页面
                <div id={'tradingView'} ref={(e)=>this._ref = e} />
                <div onClick={this.back}>后退</div>
            </div>
        )
    }

    componentDidMount() {

    }

    componentWillUnmount() {

    }

    stageDidMount(){
        Chart.init({
            dom:'tradingView',
            code:'CL',
            height:this._ref.scrollHeight,
            width:this._ref.scrollWidth
        })
    }

    stageWillUnmount(){
        Chart.exit()
    }

    back=()=>{
        this.props.navigate.goBack();
    }
}

export default createStage(App);

