import React from 'react';
import {Actor} from "../../../../core/stage/actor";
import logo from '../../assets/svg/logo.svg';
import { createStage } from '../../../../core/stage';
import {printArgs} from "../../../../lib/trace";
import {Contracts} from "../../../../pro/contract";
import {watcher, listener, spy} from "../../../../core/store";

let self = null;
class App extends Actor {

    cc = 1;
    constructor(props) {
        super(props);
        this.init();
        self = this;
        this.state = {
            work:true
        };
        console.log('2233');
    }

    render() {
        return (
            <div className="App">
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo"/>
                    {
                        this.state.work?<div className={'jump'} onClick={this.jump}>跳转</div>:null
                    }
                    <div onClick={()=>this.props.navigate.tab('info')}>跳转到info tab</div>
                </header>
            </div>
        )
    }

    jump=()=>{
        this.props.navigate.goTo('register')
    };

    @watcher(Contracts,'initial')
    init(){
        console.log('22ss')
    };

    @printArgs('演员','演员登场=>')
    stageDidMount(){
        super.stageDidMount();
        spy('test',this.second,this)
    }

    @printArgs('演员','演员退场=>')
    stageWillUnmount(){
        super.stageWillUnmount(true);
    }

    @listener('cn')
    basic(){
        console.log('sdadad');
    }

    @listener('test')
    first(){
        if(self){
            console.log(self.cc);
        }
    }

    second=()=>{
        console.log(this.cc);
    }
}


export default createStage(App);