import {createStackNavigator} from "../../../../core/navigation";
import './index.scss'
import Home from './home';
import Trade from '../trade';
import Recharge from '../recharge';
import XXPay from '../recharge/xxpay';
import Register from '../common/register';

export default createStackNavigator({
    initRouter: 'home',
    router: {
        home: {
            screen:Home,
        },
        trade: {
            screen:Trade
        },
        recharge: {
            screen:Recharge,
            tabBarVisible:false
        },
        xxpay:{
            screen:XXPay
        },
        register: {
            screen:Register
        }
    }
})