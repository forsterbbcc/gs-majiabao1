
import React from 'react'
import {Actor} from "../../../../core/stage/actor";
import { createStackNavigator } from '../../../../core/navigation';
import { createStage } from '../../../../core/stage';

// import Footer from './../footer/footer'
class App extends Actor {
    render(){
        return(
            <div className={'stage-info'}>info
                {/* <Footer/> */}
            </div>
        );
    }
} 
export default createStackNavigator({
    initRouter: 'info',
    router: {
        info: {
            screen:createStage(App),
            tabBarVisible:false
        },
    }
})