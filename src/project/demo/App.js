import React, {Component} from 'react';
import '../../lib/prototype';
import '../../pro/contract';
import Home from './view/home/index';
import Info from './view/info/info'
import { createTabNavigator } from '../../core/navigation';

import home from './assets/svg/lock.svg'
import info from './assets/svg/lock.svg'
class App extends Component {
    render() {
        return createTabNavigator({
            initRouter: 'home',
            router: {
                home: {
                    screen:Home,
                    textLabel:'首页',
                    icon:home
                },
                info: {
                    screen:Info,
                    textLabel:'资讯',
                    icon:info
                }
            },
            footer:(props)=>{
                return(
                    <div style={{background:'white',position:'fixed',left:0,right:0,bottom:0,height:58,display:'flex',justifyContent:'space-around'}}>
                        <div onClick={()=>props.goTo('home')}>111</div>
                        <div onClick={()=>props.goTo('info')}>2222</div>
                    </div>
                );
            }
        })
    }
}

export default App;
