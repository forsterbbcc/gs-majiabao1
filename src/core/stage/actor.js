import React, {Component} from 'react';
import {pullout, pulloutTarget} from "../store";

export class Actor extends Component {
    _initial = false;
    _mount = false;
    _display = false;

    constructor(props) {
        super(props);
        this._initial = true;
    }

    componentDidMount() {
        this._mount = true;
    }

    componentWillUnmount(ignore) {
        this._mount = false;
        if (!ignore) {
            pullout(this);
            if (this._listener && this._listener.length > 0) {
                let tag,scope;
                this._listener.forEach((e)=>{
                    [tag,scope] = e.split(':');
                    pulloutTarget(tag,scope)
                });
                this._listener = [];
            }
        }
    }

    stageDidMount() {
        this._display = true;
    }

    stageWillUnmount(ignore = false) {
        this._display = false;
        if (!ignore) {
            pullout(this);
            if (this._listener && this._listener.length > 0) {
                let tag,scope;
                this._listener.forEach((e)=>{
                    [tag,scope] = e.split(':');
                    pulloutTarget(tag,scope)
                });
                this._listener = [];
            }
        }
    }

    _storeListenerName(name) {
        this._listener = this._listener || [];
        this._listener.push(name)
    }

    updateWhileMount(state) {
        if (this._initial) {
            if (this._mount) {
                this.setState(state)
            }
        }
    }

    updateWhileDisplay(state) {
        if (this._initial) {
            if (this._display) {
                this.setState(state)
            }
        }
    }
}