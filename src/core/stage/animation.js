import {invariant} from "../../lib/utils";

const INITIALIZATION = 0; //初始化
const WILL_APPEAR = 1; //初始化完成，但是并不在屏幕中的状态
const DID_APPEAR = 2; //当前屏幕正在显示的状态
const IN_DEEP = 3; //在舞台幕后的状态
const ANIMATION = 4; //组件正在动画中

class Animation {
    //初始样式
    initialStyle = '_stage-next _stage-show';

    moveInStyle = '_stage-next _move-in';

    onStyle = '_stage-on';

    backStyle = '_stage-prev';

    tempBackStyle = '_stage-prev _stage-show';

    moveIn(stage, callback, animated) {
        invariant(typeof stage === 'object', '操作实例对象类型错误');
        invariant(typeof callback === 'function', '动画回调类型错误或不存在');
        stage.setStateWhileMount({style: '_stage-next _stage-show'}, () => {
            setTimeout(() => {
                stage.setStateWhileMount({style: '_stage-next _move-in'}, () => {
                    setTimeout(() => {
                        stage.setStateWhileMount({style: this.onStyle});
                        stage._setStatus(DID_APPEAR);
                        callback.call(stage);
                        animated();
                    }, 400);
                })
            }, 100)
        });
    }

    fadeIn(stage, callback) {
        invariant(typeof stage === 'object', '操作实例对象类型错误');
        invariant(typeof callback === 'function', '动画回调类型错误或不存在');
        stage.setStateWhileMount({style: this.tempBackStyle}, () => {
            setTimeout(() => {
                stage.setStateWhileMount({style: this.onStyle});
                stage._setStatus(DID_APPEAR);
                callback.call(stage);
            }, 500)
        })
    }

    moveOut(stage, callback) {
        invariant(typeof stage === 'object', '操作实例对象类型错误');
        invariant(typeof callback === 'function', '动画回调类型错误或不存在');
        stage.setStateWhileMount({style: '_stage-out '}, () => {
            setTimeout(() => {
                stage.setStateWhileMount({style: '_stage-out _move-out'}, () => {
                    setTimeout(() => {
                        stage.setStateWhileMount({style: `_stage-out _stage-hide`});
                        stage._setStatus(DID_APPEAR);
                        callback();
                    }, 400)
                });
            }, 100)
        })
    }

    fadeOut(stage, callback) {
        invariant(typeof stage === 'object', '操作实例对象类型错误');
        invariant(typeof callback === 'function', '动画回调类型错误或不存在');
        stage.setStateWhileMount({style: this.tempBackStyle}, () => {
            setTimeout(() => {
                stage.setStateWhileMount({style: this.backStyle});
                stage._setStatus(IN_DEEP);
                if (!!callback) {
                    callback.call(stage);
                }
            }, 500)
        })
    }

    restoreIn(stage) {
        invariant(typeof stage === 'object', '操作实例对象类型错误');
        stage.setStateWhileMount({style: this.backStyle});
        setTimeout(() => {
            stage._setStatus(IN_DEEP);
        }, 500)
    }
}

export default new Animation();