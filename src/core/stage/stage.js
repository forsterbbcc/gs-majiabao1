import React, {Component} from 'react'
import './stage.scss';
import animation from './animation'
import {printArgs, printDir} from "../../lib/trace";
import {safeValue} from "../../lib/data/safety";
import { spy, pullout } from '../store';


export default function CreateStage(View) {
    return class Temp extends Component {

        state = {
            actor:null
        };

        render() {
            return (
                <Stage actor={this.state.actor} {...this.props}>
                    <View {...{navigate: this.props.navigate}} ref={(e)=>this.actor = e}/>
                </Stage>
            )
        }

        componentDidMount(){
            this.setState({actor:this.actor})
        }
    }
}

const INITIALIZATION = 0; //初始化
const WILL_APPEAR = 1; //初始化完成，但是并不在屏幕中的状态
const DID_APPEAR = 2; //当前屏幕正在显示的状态
const IN_DEEP = 3; //在舞台幕后的状态
const ANIMATION = 4; //组件正在动画中


class Stage extends Component {

    status = INITIALIZATION;
    _mount = false;

    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <div className={`_stage ${this.state.style}`}>
                {this.props.children}
            </div>
        )
    }

    @printDir('stage', '舞台物理渲染完成=>')
    componentDidMount() {
        const {reportNewComponent,navigate,name} = this.props;
        
        this._mount = true;
        /**
         * 保存当前舞台实例
         * @type {Stage}
         */
        navigate.stage[name] = this;
        /**
         * 报告物理渲染结束
         */
        reportNewComponent(name,this);
    }

    componentWillUnmount() {
        this._mount = false;
    }

    @printArgs('stage','舞台加载完成=>是否初始舞台')
    stageDidMount(initial) {
        /**
         * 初始化舞台状态
         */
        if (initial) {
            this.setStateWhileMount({
                style:animation.onStyle
            });
            this.awakeActorDidMount();
            this._setStatus(DID_APPEAR);
        } else{
            this._setStatus(WILL_APPEAR);
        }
        
    }

    /**
     * 唤醒演员的进入事件
     */
    awakeActorDidMount(){
        const {name} = this.props;
        setTimeout(()=>{
            const {actor} = this.props;
            if(safeValue(actor)){
                if(safeValue(actor.stageDidMount)){
                    actor.stageDidMount(name);
                }else{
                    console.warn(`演员的进入事件未设置=>${name}, stageDidMount in actor=>${name} is not define`)
                }
            }else {
                console.warn('演员载入出错');
            }
        },0);
    }

    /**
     * 唤醒演员的退出事件
     */
    awakeActorWillUnmount(){
        const {name} = this.props;
        const {actor} = this.props;
        if(safeValue(actor)){
            pullout(this);
            if(safeValue(actor.stageWillUnmount)){
                actor.stageWillUnmount(name);
            }else{
                console.warn(`演员的退出事件未设置=>${name}, stageWillUnmount in actor=>${name} is not define`)
            }
        }else {
            console.warn('演员载入出错');
        }
    }


    /**
     * 场景移入事件
     * @param {Function} callback
     * @param {Function} animated
     * @private
     */
    _moveIn(callback,animated) {
        if (this.status === WILL_APPEAR) {
            this._setStatus(ANIMATION);
            animation.moveIn(this, callback,animated);
        }
    }

    /**
     *
     * @param {Function} callback
     * @private
     */
    _moveOut(callback) {
        if (this.status === DID_APPEAR) {
            this._setStatus(ANIMATION);
            animation.moveOut(this, callback);
        }
    }

    _fadeIn(callback){
        if(this.status === IN_DEEP){
            this._setStatus(ANIMATION);
            animation.fadeIn(this,callback);
        }
    }

    _fadeOut(callback){
        if(this.status === DID_APPEAR){
            this._setStatus(ANIMATION);
            animation.fadeOut(this,callback)
        }
    }

    _restoreIn(){
        if(this.status === WILL_APPEAR){
            this._setStatus(ANIMATION);
            animation.restoreIn(this);
        }
    }

    /**
     *
     * 设置舞台的状态
     * @param {Number} status
     */
    _setStatus(status) {
        this.status = status;
    }

    /**
     * 安全的setState
     *
     * @param {*} obj setState的对象
     * @param {Function} [callback] setState成功后的回调
     * @returns
     */
    setStateWhileMount(obj, callback) {
        if (!this._mount) return;
        this.setState(obj, callback);
    }
}