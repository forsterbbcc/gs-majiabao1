import _createStage from './stage'
import {Actor} from "./actor";

/**
    * @method
    * @description 把组件打包为舞台组件
    */
export function createStage(child) {
    try{
        if(child.prototype instanceof Actor === false){
            throw new Error(`传入的页面,不是一个Actor`)
        }
        return _createStage(child)
    }catch (err){
        console.error(err)
    }
}