/**
 *@author wilde create at 2018/11/21 10:21 AM
 */
import React, {Component} from 'react';
import {printArgs} from "../../lib/trace";
import {safeValue} from "../../lib/data/safety";
import _storage from './navigatorStorage';
import {_injectNavigate} from "./navigate";
import {spy} from "../store";

/**
 *
 * @param {Object} props
 * @returns {*}
 * @constructor
 */
export default function CreateStackNavigator(props) {
    try {
        const {initRouter, router} = props;
        if (!initRouter) console.warn(new Error('未设置初始路由'));
        if (!router) return new Error('尚未配置任何路由');
        for (let [key, val] of Object.entries(router)) {
            if (val.tabBarVisible !== false) router[key].tabBarVisible = true;
            if (val.live !== true) router[key].live = false;
        }
    } catch (err) {
        console.error(err);
    }
    return function (argv) {
        return (<StackNavigator {...props} {...argv}/>);
    }
}

class Navigate {
    constructor(props, stack) {
        this._stack = stack;
        this.useStage = true;
        this.destroy = null;
        this.store = null;

        this.current = props.initRouter;

        this.history = [];
        /**
         * 保存当前所有的stage实例
         * @type {{}}
         */
        this.stage = {};
        /**
         * 存储dom在root中的顺序,以求在不使当前dom重绘的情况下,操纵所有页面
         * @type {Array}
         */
        this.live = [];

        /**
         * 创造的新页面
         * @type {null}
         */
        this.create = null;
        /**
         * 恢复的新页面
         * @type {null}
         */
        this.restore = null;

        this.tab = props.tab;
        this.setBarLayerMove = props.setBarLayerMove;
        this.setBarVisible = props.setBarVisible;

        Object.assign(this, props);
        if (!this.current) {
            const [def] = Object.keys(this.router);
            this.current = def;
        }
        this.history.push(this.current);
        this.live.push(this.current);

        this._detectNewComponent = this._detectNewComponent.bind(this);

        /**
         * 状态锁 用来锁定路由操作
         * @type {boolean}
         */
        this.lock = false;

        /**
         * 临时寄存器
         * @type {{}}
         */
        this.state = {};
    }

    _routing = (props) => {
        return (
            this.live.map((e, key) => {
                if (!e) {
                    return <div key={key}/>
                } else {
                    return this._render(props, e, key)
                }
            })
        )
    };

    _render = (props, e, key) => {
        const Element = this.router[e].screen;
        return <Element name={e}
                        key={key}
                        reportNewComponent={this._detectNewComponent}
                        tabBarVisible={this.router[e].tabBarVisible}
                        live={this.router[e].live}
                        {...props}/>
    };

    /**
     * 以创造的形式增加新页面
     * @param {String} route
     * @private
     */
    _create = (route) => {
        if (this.live.length === 3) {
            this.live.replace(this.store, route);
        } else {
            if (this.live.indexOf(null) !== -1) {
                this.live.replace(null, route);
            } else {
                this.live.push(route)
            }
        }
        this.history.push(route);
        this.create = route;
    };

    /**
     * 以恢复的形式增加新页面
     * @private
     */
    _restore = () => {
        this.history.pop();
        if (this.history.length > 1) {
            const restore = this.history[this.history.length - 2];
            this.live.replace(null, restore);
            this.restore = restore;
            return true;
        } else {
            return false;
        }
    };

    /**
     * 舞台创建检测器
     * @param name
     * @param target
     * @private
     */
    @printArgs('navigator', '检测到新舞台创建=>')
    _detectNewComponent(name, target) {
        if (this.create === name) {
            this._setGoToPlan(name);
            this.create = null;
            target.stageDidMount(false);
            this._doAnimateTypeGoTo(target);
        } else if (this.restore === name) {
            this._setGoBackPlan(name);
            this.restore = null;
            target.stageDidMount(false);
            this._doAnimateTypeGoBack(target);
        } else {
            target.stageDidMount(true);
        }
    };

    /**
     * 设置Tab Bar开始路由动画
     * @private
     */
    _setTabBarAnimationStart = (forward = 'in', delay) => {
        let original, next;
        if (forward === 'in') {
            original = this.router[this.store].tabBarVisible;
        } else if (forward === 'out') {
            original = this.router[this.destroy].tabBarVisible;
        }
        next = this.router[this.current].tabBarVisible;
        this.setBarLayerMove(forward, original, next, 1, this.index, delay);
    };

    /**
     * 设置Tab Bar结束路由动画
     * @private
     */
    _setTabBarAnimationEnd = (forward = 'in') => {
        this.lock = false;
        let original, next;
        original = this.router[this.destroy || this.store].tabBarVisible;
        next = this.router[this.current].tabBarVisible;
        this.setBarLayerMove(forward, original, next, 2, this.index);
    };

    /**
     * 操作舞台实例执行GoTo类动画
     * @private
     */
    _doAnimateTypeGoTo(target) {
        this._setTabBarAnimationStart();
        /**
         * 操作界面进行移入动画
         */
        target._moveIn(target.awakeActorDidMount, this._setTabBarAnimationEnd);

        if (safeValue(this.store)) {
            const store = this.stage[this.store];
            if (safeValue(store)) {
                store._fadeOut(store.awakeActorWillUnmount);
            }
        }
        if (safeValue(this.destroy)) {
            const destroy = this.stage[this.destroy];
            if (safeValue(destroy)) {
                this.live.replace(this.destroy, null);
                this._stack.rooting();
            }
        }
    }

    /**
     * 操作舞台实例执行GoBack类动画
     * @private
     */
    _doAnimateTypeGoBack(target) {
        this._setTabBarAnimationStart('out', () => {
            if (safeValue(this.destroy)) {
                const destroy = this.stage[this.destroy];
                if (safeValue(destroy)) {
                    this.live.replace(this.destroy, null);
                    destroy._moveOut(() => {
                        this._setTabBarAnimationEnd('out');
                        destroy.awakeActorWillUnmount();
                        this._stack.rooting();
                    });
                }
            }
        });
        if (safeValue(this.current)) {
            const current = this.stage[this.current];
            if (safeValue(current)) {
                current._fadeIn(current.awakeActorDidMount)
            }
        }
        if (target) {
            target._restoreIn()
        }
    }

    _setGoToPlan(name) {
        /**
         * 计划销毁
         */
        this.destroy = this.store;
        /**
         * 存储当前页
         */
        this.store = this.current;
        /**
         * 将新舞台设置为当前舞台
         */
        this.current = name;
    }

    _setGoBackPlan(name) {
        /**
         * 计划退出当前舞台并销毁
         */
        this.destroy = this.current;
        /**
         * 将备用舞台设为当前舞台
         */
        this.current = this.store;

        /**
         * 将新生成的舞台设为备用舞台
         */
        this.store = name || null;
    }

    _setGoTopPlan(name) {
        /**
         * 计划退出当前舞台并销毁
         */
        this.destroy = this.current;
        /**
         * 将最底部舞台设为当前舞台
         */
        this.current = name;

        /**
         * 将新生成的舞台设为备用舞台
         */
        this.store = null;
    }

    /**
     * 跳转到 Stack中指定的路由
     * @param {String} route
     * @param {Object} [state]
     */
    goTo = (route, state = {}) => {
        if (typeof route === 'string') {
            if (!!this.router[route]) {
                if (this.current !== route) {
                    _storage.insertStackState(route, state);
                    if (this.history.length > 1 && this.history[this.history.length - 2] === route) {
                        this.goBack();
                    } else {
                        if (this.lock) return;
                        this.lock = true;
                        this._create(route);
                        this._stack.rooting();
                    }
                } else {
                    console.warn('请不要试图前往相同的页面 %s', route)
                }
            } else {
                console.warn('试图前往的路由 %s 并不存在', route)
            }
        } else {
            console.warn('路由跳转接受参数类型为 String')
        }
    };

    /**
     * 退回上一个页
     */
    goBack = () => {
        if (this.history.length > 1) {
            if (this.lock) return;
            this.lock = true;
            if (this._restore()) {
                this._stack.rooting();
            } else {
                this._setGoBackPlan();
                this._doAnimateTypeGoBack();
            }
        } else {
            console.warn('当前界面已是路由顶点')
        }
    };

    /**
     * 返回至路由树顶点
     */
    popToTop = () => {
        if (this.history.length > 2) {
            if (this.lock) return;
            this.lock = true;
            const top = this.history[0];
            this.history = [top];
            this.live.replace(this.store, top);
            this._setGoTopPlan(top);
            this._doAnimateTypeGoBack();
        } else {
            this.goBack();
        }
    };

    get state() {
        return _storage.getStackState(this.history[this.history.length - 1]);
    }

    /**
     *
     * @param value
     */
    set state(value) {
        _storage.insertStackState(this.history[this.history.length - 1], value);
    }
}

class StackNavigator extends Component {

    constructor(props) {
        super(props);
        this.navigation = new Navigate(props, this);
        _injectNavigate(this.navigation)
    }

    render() {
        return (
            <this.navigation._routing {...{navigate: this.navigation}} />
        )
    }

    /**
     * 重绘页面
     */
    rooting = () => {
        this.setState({})
    }
}
