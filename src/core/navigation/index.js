import _createStackNavigator from './stackNavigator';
import _createTabNavigator from './tabNavigator';
import {_withNavigation} from './navigate';
import _storage from './navigatorStorage';

import {develop} from "../../lib/trace";
develop(_storage);


export function withNavigation(component) {
    return _withNavigation(component)
}

/**
 *
 * @param {Object} argv
 * @returns {*}
 */
export function createStackNavigator(argv) {
    if(!!argv &&  typeof argv === 'object'){
        const def = {
            tab:function (){
                console.warn('请传tab参数');
            }
        };
        Object.assign(def,argv);
        _storage.initStack(Object.keys(argv.router));
        return _createStackNavigator(def)
    }else{
        console.error(new Error('初始化 stackNavigator 时未传入一个可用的配置'));
        return null
    }
}

/**
 *
 * @param {Object} argv
 * @param {String} argv.initRouter
 * @returns {*}
 */
export function createTabNavigator(argv) {
    if(!!argv &&  typeof argv === 'object'){
        return _createTabNavigator(argv)
    }else{
        console.error(new Error('初始化 tabNavigator 时未传入一个可用的配置'));
        return null
    }
}