export default {
    name: 'state',
    stack: {},
    insertStackState(router, state = {}) {
        this.stack[router] = state;
    },
    initStack(array = []) {
        array.forEach((key) => {
            this.stack[key] = {};
        });
    },
    getStackState(router) {
        return this.stack[router];
    }
}