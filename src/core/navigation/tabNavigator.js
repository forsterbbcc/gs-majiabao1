/*
 * @Description: 
 * @Author: Dixon Wong
 * @Date: 2019-02-19 09:13:19
 * @LastEditors: Dixon Wong
 * @LastEditTime: 2019-03-28 15:25:22
 */

import React, {Component} from 'react'
import _storage from './navigatorStorage';
import './tab.scss'
import Footer from './../../components/common/footer/footer';
import {exposure} from '../store';
import {_onTabChange, _navigate} from "./navigate";
import {EVENT} from '../../pro/event';


export default function CreateTabNavigator(props) {
    try {
        const {initRouter, router} = props;
        if (!initRouter) console.warn(new Error('未设置初始路由'));
        if (!router) return new Error('尚未配置任何路由')
    } catch (err) {
        console.error(err);
    }
    return (<TabNavigator {...props} />);
}

class TabNavigator extends Component {

    /**
     * bar 切换进程记录
     * @type {null}
     */
    process = null;
    tabClass = {};

    constructor(props) {
        super(props);
        this.state = {
            show: this.props.initRouter,
            footer: this.props.footer,
            tabBarVisible: '',
            lastTab: ''
        };
        _onTabChange(this.props.initRouter);
        for (let key of Object.keys(this.props.router)) {
            this.tabClass[key] = ''
        }
    }

    renderFooter(key, o) {
        const {footer: Custom, router} = this.props;
        if (Custom) {
            return (
                <Custom index={key} source={router} className={`${this.tabClass[key]} ${!o ? 'foo-z-3' : ''}`}
                        show={this.state.show} goTo={this.goTo}/>
            )
        } else {
            return (
                <Footer index={key} className={`${this.tabClass[key]} ${!o ? 'foo-z-3' : ''}`} show={this.state.show}
                        goTo={key => this.goTo(key)} source={this.props.router}/>
            )
        }
    }

    render() {
        return (
            <div>
                {
                    Object.keys(this.props.router).map((key, index) => {
                        let o = this.props.router[key].tabBarVisible;
                        if (o === undefined) o = true;
                        let T = this.props.router[key].screen;
                        let live = this.props.router[key].live;

                        let style = key === this.state.show?'showTab':(live?'liveTab':'hideTab');


                        return (
                            <div key={index} className={`tabBox ${style}`}>
                                <T index={key} tab={this.goTo} setBarLayerMove={this.setBarLayerMove}/>
                                {this.renderFooter(key, o)}
                            </div>
                        );
                    })
                }
            </div>
        )
    }

    setBarLayerMove = (forward, from, to, step, index, callback) => {
        if (index !== this.state.show) return;
        if (step === 1) {
            this.barLayerMoveStart(forward, from, to, callback)
        } else if (step === 2) {
            this.barLayerMoveEnd(forward, from, to)
        }

    };

    barLayerMoveStart(forward = 'in', from, to, callback) {
        const formIn = {
            'true-false': 'foo-z1',
            'true-true': 'foo-z3',
            'false-false': 'foo-z-3'
        };
        const formOut = {
            'false-true': 'foo-z1',
            'true-true': 'foo-z3',
            'false-false': 'foo-z-3'
        };
        let attribute = `${from}-${to}`;
        if (forward === 'in') {
            this.tabClass[this.state.show] = formIn[attribute];
            this.setState({}, () => {
                callback && callback();
            })
        } else if (forward === 'out') {
            this.tabClass[this.state.show] = formOut[attribute];
            this.setState({}, () => {
                callback && callback();
            })
        }
    };

    barLayerMoveEnd(forward = 'in', from, to) {
        const formIn = {
            'true-false': 'foo-z-1',
            'true-true': 'foo-z1',
            'false-false': 'foo-z-1'
        };
        const formOut = {
            'false-true': 'foo-z1',
            'true-true': 'foo-z1',
            'false-false': 'foo-z-1'
        };
        let attribute = `${from}-${to}`;
        if (forward === 'in') {
            this.tabClass[this.state.show] = formIn[attribute];
            this.setState({})
        } else if (forward === 'out') {
            this.tabClass[this.state.show] = formOut[attribute];
            this.setState({})
        }
    }

    /**
     *
     * @param {string} tab
     * @param {object} [state]
     */
    goTo = (tab, state) => {
        if (!!this.props.router[tab].auth && !EVENT.Account.isLogin) return _navigate().goTo('login');

        if (!this.props.router[tab]) return;
        _onTabChange(tab);
        _storage.insertStackState(tab, state);
        this.setState({
            show: tab
        }, () => {
            exposure('changeTab', {show: tab})
        })
    };

    /**
     * 重绘页面
     */
    rooting = () => {
        this.setState({})
    }
}