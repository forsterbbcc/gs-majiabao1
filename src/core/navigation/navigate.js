/**
 *@author wilde create at 2019/3/6 3:15 PM
 */
import React from 'react';

const _stock = {};

let index = null;

const Navigate = {
    index:index,
    get current(){
        return _stock[index].current
    },
    get state(){
        return _stock[index].state
    },
    set state(val){
        _stock[index].state = val
    },
    goBack(){
        _stock[index].goBack();
    },
    goTo(url,state){
        _stock[index].goTo(url,state);
    },
    popToTop(){
        _stock[index].popToTop();
    },
    tab(url,state){
        _stock[index].tab(url,state)
    }
};

export function _onTabChange(tab) {
    index = tab;
    Navigate.index = tab;
}

/**
 * 讲stack的navigate注入缓存
 * @param obj
 * @private
 */
export function _injectNavigate(obj) {
    _stock[obj.index] = obj;
}

export function _withNavigation(App) {
    return function (props) {
        return (
            <App navigate={Navigate} {...props}/>
        )
    }
}

export function _navigate(){
    return Navigate;
}