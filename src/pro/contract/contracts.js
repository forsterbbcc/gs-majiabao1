import {getSearch} from "../../lib/utils";
import {safeValue} from "../../lib/data/safety";
import {Req} from "../network/Req";
import {exposure, inspectable} from "../../core/store";
import {decodeFromWeb} from "../network/encrypt";
import {JsonListMerge as quoteAdd} from "../network/Jsonp";
import {JsonListMerge as chartAdd} from "../chartTV/chart";
import {EVENT} from "../event";
import { JsonListMerge as chartIQAdd } from "../chartiq/chartIQ";

export default class Contracts {
    initial = false;
    constructor() {
        this.name = 'Contracts';
        this.quoteList = [];
        this.domestic = {};
        this.domesticArray = [];
        this.stock = {};
        this.stockArray = [];
        this.foreign = {};
        this.foreignArray = [];
        this.total = {};
        this.totalArray = [];
        this.hot = ['CL', 'IF', 'HSI', 'DAX', 'HG', 'RB'];
        this.new = ['SC', 'YM', 'NQ', 'NK'];
        //todo 竞猜
        this.betArray = [];
        this.serviceTime = 0;
        this.index = '';
    }

    async init(extend) {
        try {
            const res = await Req({url: '/api', data: getSearch()});
            let {quoteDomain, domesticCommds, foreignCommds, stockIndexCommds, contracts, quizCommodity, session, time} = res;
            this.serviceTime = +time;
            //todo 解密备用行情地址
            let book = decodeFromWeb(quoteDomain);
            book = book.split(';');
            quoteAdd(book);
            chartAdd(book[0]);
            chartIQAdd(book[0]);

            for (let e of domesticCommds) {
                this.domestic[e.code] = e;
                this.domesticArray.push(e);
            }
            for (let e of foreignCommds) {
                this.foreign[e.code] = e;
                this.foreignArray.push(e);
            }
            for (let e of stockIndexCommds) {
                this.stock[e.code] = e;
                this.stockArray.push(e);
            }

            this.betArray = quizCommodity || [];
            this.totalArray = [].concat(this.domesticArray, this.foreignArray, this.stockArray);
            this.quoteList = JSON.parse(contracts);

            for (let [code, obj] of Object.entries(this.domestic)) {
                let c = this.quoteList.find((c) => {
                    return c.startsWith(code);
                });
                if (!!c) {
                    obj.contract = c;
                    this.injectSession(obj, session);
                    this.total[c] = obj;
                }
            }
            for (let [code, obj] of Object.entries(this.foreign)) {
                let c = this.quoteList.find((c) => {
                    return c.startsWith(code);
                });
                if (!!c) {
                    obj.contract = c;
                    this.injectSession(obj, session);
                    this.total[c] = obj;
                }
            }
            for (let [code, obj] of Object.entries(this.stock)) {
                let c = this.quoteList.find((c) => {
                    return c.startsWith(code);
                });
                if (!!c) {
                    obj.contract = c;
                    this.injectSession(obj, session);
                    this.total[c] = obj;
                }
            }
            this.quoteList = this.totalArray.map((c) => c.contract);
            this.index = this.foreignArray[0].contract;
            if(extend) extend();
            this.extended();
            this.initial = true;
            EVENT.Trade._insertContracts(this.total);
            exposure('contractsInitial')
        } catch (err) {
            console.warn(err);
            if (err === null || err === '请求超时' || err.code !== undefined) {
                setTimeout(() => {
                    this.init(extend)
                }, 500)
            }
        }
    }

    extended(){

    }

    /**
     * 像商品数据中注入session数据
     * @param c
     * @param session
     */
    injectSession(c, session) {
        if (safeValue(session)) {
            const {code} = c;
            if (safeValue(session[code])) {
                c.session = session[code]
            }
        }
    }

    isHot(contract) {
        let code = this.total[contract].code;
        return this.hot.includes(code);
    }

    isNew(contract) {
        let code = this.total[contract].code;
        return this.new.includes(code);
    }

    /**
     * 通过商品CODE或合约号查询合约数据
     * @param code
     * @returns {*}
     */
    getContractByCode(code) {
        if (!!code) {
            if(/\d+/.test(code)){
                return this.total[code];
            }else{
                return this.totalArray.find((e) => {
                    return e.code === code
                })
            }
        }
        return null;
    }

    /**
     * 行情及页面获取自身合约号的方法
     * @param component
     * @returns {boolean || Contracts}
     */
    getContract(component) {
        if (this.initial) {
            let o = component.props.location.state.contract;
            if (!o) {
                [o] = this.foreignArray;
                o = o.contract;
                component.props.location.state.contract = o;
            }
            return o;
        } else {
            return false;
        }
    }
}