import _contracts from "./contracts";
import _data from './data';
import _quote from './quote';
import {develop} from "../../lib/trace";
import {exposure} from "../../core/store";

const Contracts = new _contracts();
const Data = new _data(Contracts);
const Quote = new _quote(Contracts);

develop(Contracts);
/**
 * Contract本身未扩展的情况下通过init传入回调来初始化data和quote
 *
 * Contract本身有扩展的情况下,通过覆盖Contract.extended来完成扩展
 */
Contracts.init(()=>{
    Data.init();
    Quote.init();
}).catch(()=>{console.warn(`初始化失败,次数${console.count('contracts')}`)});
export {
    Contracts,
    Data,
    Quote
}
