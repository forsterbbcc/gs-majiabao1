import {Jsonp} from "../network/Jsonp";
import {Rest} from "../util";
import {exposure} from "../../core/store";
import {EVENT} from "../event";

export default class Data {

    initial = false;

    constructor(e) {
        this.name = 'Data';
        this.contracts = e;
        this.domesticBrief = [];
        this.foreignBrief = [];
        this.stockBrief = [];
        this.allBrief = [];
        this.total = {};
        this.strap = '';
        this.plan = null;
        this.callbackList = [];
        this.interval = 2000;
    }

    init() {
        for (let {contract, name, code} of this.contracts.domesticArray) {
            if (!!contract) {
                let o = {code: contract, name: name, price: null, rate: null, isUp: null, difference:null, goodsCode:code};
                this.domesticBrief.push(o);
                this.total[contract] = o;
            }
        }
        for (let {contract, name, code} of this.contracts.foreignArray) {
            if (!!contract) {
                let o = {code: contract, name: name, price: null, rate: null, isUp: null, difference:null, goodsCode:code};
                this.foreignBrief.push(o);
                this.total[contract] = o;
            }
        }
        for (let {contract, name, code} of this.contracts.stockArray) {
            if (!!contract) {
                let o = {code: contract, name: name, price: null, rate: null, isUp: null, difference:null, goodsCode:code};
                this.stockBrief.push(o);
                this.total[contract] = o;
            }
        }

        for (let {contract} of this.contracts.totalArray) {
            this.allBrief.push(this.total[contract]);
        }


        this.strap = this.contracts.quoteList.join(',');
        this.initial = true;
    }

    start(callback) {
        if (!callback)
            return console.error('缺少回调字符串');

        if (this.callbackList.length > 0) {
            if (this.callbackList.includes(callback))
                return;
            this.callbackList.push(callback);
        } else {
            this.callbackList.push(callback);
            this.inquiryAll();
        }
    }

    end(callback) {
        if (!callback)
            return;

        this.callbackList.remove(callback);
        if (this.callbackList.length === 0) {
            clearTimeout(this.plan);
            this.plan = null;
        }
    }

    _callback(res) {
        for (let [name, isUp, price, prev] of res) {
            this.total[name].price = price;
            this.total[name].prev = prev;
            this.total[name].isUp = isUp > 0;
            //todo 百分比
            this.total[name].rate = (() => {
                if (isUp > 0) {
                    return `+${(((price - prev) / prev) * 100).toFixed(2)}%`
                } else {
                    return `-${(((prev - price) / price) * 100).toFixed(2)}%`
                }
            })();
            //todo 差额
            this.total[name].difference=(()=>{
                if(isUp > 0){
                    return `+${(price - prev).toFixed(2)}`
                } else {
                    return `-${(prev - price).toFixed(2)}`
                }
            })();
            this.total[name].isOpen = Rest.isOpening(this.contracts.total[name]);
        }
        EVENT.Trade._backdoorForData(this.total);
        EVENT.Game.Quiz._backdoorForData(this.total);
        if (this.callbackList.length > 0) {
            for (let v of this.callbackList) {
                exposure(v)
            }
            this.plan = setTimeout(() => this.inquiryAll(), this.interval);
        }
    }

    async inquiryAll() {
        try {
            const res = await Jsonp({
                url: '/quote.jsp',
                type: 'POST',
                data: {
                    callback: '?',
                    code: this.strap,
                    _: new Date().getTime(),
                    simple: true
                },
                timeout: 4000
            });
            this._callback(res);
        } catch (err) {
            if (err === null) {
                setTimeout(() => {
                    this.inquiryAll();
                }, 4000)
            } else {
                console.warn(err);
            }
        }
    }
}