import {getPlatform} from "../../lib/utils";
import {Req} from "../network/Req";
import {LinkTo} from "../../core/native/native";
import alias from './alias';

/**
 * type 数据源类型 input-输入  select-选择  fixed-定值
 * placeholder 占位符
 * style input-only 输入框的类型 number tel text password
 * length input-only 输入的长度限制  仅 text password tel生效
 * float input-only 限制输入的内容是否包含小数点
 * nonzero input-only 限制输入的内容最后一位是否不为0
 * min input-only number-tel only 限制输入的数字的最小值
 * max input-only number-tel only 限制输入的数字的最大值
 * chn 中文限制 true-只能中文  false-非中文
 */

export const PAYMENT = {
    Alert: null,
    list: [],
    route: {},
    payFirst: 0,
    setAlert(func) {
        if (typeof func === 'function') {
            this.Alert = func;
        }
    },
    _caution(msg) {
        if (this.Alert) {
            this.Alert(msg);
        } else {
            console.error(msg);
        }
    },
    alias(name) {
        return alias[name] || name;
    },
    _updatePayment() {
        return Req({
            url: '/api/pay/recharge.htm',
            type: 'POST',
            data: {
                action: 'getPayList',
                switchType: 1,
                platform: getPlatform()
            }
        })
    },
    _package(data,local) {
        let e;
        data.forEach(({id,area,description,name,url}) => {
            e = {id,area,subtitle:description,name};
            let [route,key] = url.split('?');
            if(local[route]){
                e = Object.assign(e,local[route]);
                key = key.split('&');
                if (key.length > 0) {
                    key.forEach((ele) => {
                        if (ele) {
                            const [key, val] = ele.split('=');
                            e.param[key] = {
                                type: 'fixed',
                                value: val
                            }
                        }
                    })
                }
                this.list.push(e);
                this.route[id] = e;
            }
        });
        return this.list;
    },
    getConfig(id) {
        return Object.assign({}, this.route[id])
    },
    getList() {
        return new Promise(async (resolve,reject) => {
            try {
                if (this.list.length > 0) return resolve(this.list);
                const branch = this.alias(process.env.BRANCH);
                const {default:_} = await import(/* webpackChunkName: "payment" */'./branch/' + branch);
                const {payList, payFirst} = await this._updatePayment();
                this.payFirst = payFirst;
                resolve(this._package(payList,_))
            } catch (err) {
                reject(err)
            }
        })
    },
    isFirstPay() {
        return this.payFirst;
    },
    verify(obj) {
        return Object.entries(obj).every(([key, o]) => {
            if (o.type === 'input') {
                if (o.value === '') {
                    this._caution(`请输入${o.title}`);
                    return false;
                }
                if ((o.style === 'number' || o.style === 'tel') && o.min !== undefined && o.min > 0) {
                    if (o.value < o.min) {
                        this._caution(`${o.title}最低${o.min}`);
                        return false;
                    }
                }
                if ((o.style === 'number' || o.style === 'tel') && o.max !== undefined && o.max > 0) {
                    if (o.value > o.max) {
                        this._caution(`${o.title}最高${o.max}`);
                        return false;
                    }
                }
                if (o.float !== undefined) {
                    if ((o.value.toString().indexOf('.') !== -1) !== o.float) {
                        this._caution(`${o.title}${o.float ? '必须' : '不能'}含有小数`);
                        return false;
                    }
                }
                if (o.nonzero !== undefined && o.nonzero) {
                    if (o.value.toString().slice(-1) === '0') {
                        this._caution(`${o.title}最后一位不能为0`);
                        return false;
                    }
                }
                if (o.chn !== undefined) {
                    o.value = o.value.replace(/\r\n/g, '');
                    let pattern = /^([u4e00-u9fa5]|[ufe30-uffa0])*$/gi;
                    let other = /\d/g;
                    if (o.chn) {
                        console.log(other.test(o.value));
                        if (pattern.test(o.value)) {
                            this._caution(`${o.title}请输入中文`);
                            return false
                        }
                    } else {
                        if (!pattern.test(o.value)) {
                            this._caution(`${o.title}请不要输入中文`);
                            return false
                        }
                    }
                }
                return true
            } else if (o.type === 'select') {
                if (o.value === '') {
                    this._caution(`请选择${o.title}`);
                    return false;
                }
                return true
            } else {
                return true
            }
        })
    },
    fast(obj) {
        return new Promise(async (resolve, reject) => {
            try {
                let o = {};
                for (let [key, {value}] of Object.entries(obj.param)) {
                    if (key === 'money') {
                        o[key] = Number(value);
                    } else {
                        if (value === 'origin') value = window.location.origin;
                        o[key] = value
                    }
                }
                const data = await Req({
                    url: obj.url,
                    type: 'POST',
                    data: o,
                    animate: true
                });
                if (data.html) {
                    document.write(data.html)
                } else if (data.redirectURL) {
                    if (window.isSuperman) {
                        LinkTo(data.redirectURL)
                    } else {
                        window.location.href = data.redirectURL
                    }
                }
                resolve();
            } catch (err) {
                reject(err)
            }
        });
    }
};

