
let jsonList = null;
export function JsonListMerge(address) {
    jsonList = address;
}
const ChartIQ = {
    stxx:null,
    contract:null,
    initial:false,
    timeUnit:null,
    interval:null,
    period:null,
    type:'slice',
    domName:null,
    /**
     * @param {*}  options
     * 初始化行情图
     * 
     * 添加tradeVolume属性{upVolume,downVolume,upBorder,downBorder}
     * 参数则是对应属性的颜色
     */
    init(options){
        if (!options.domName)
            return console.error('缺少chart容器ID');

        if (!options.contract)
            return console.error('缺少初始商品ID');

        this.contract = options.contract;
        
        let dom = document.querySelector(options.domName);
        this.domName = options.domName;
        if (this.stxx === null) {
            this.stxx=new window.CIQ.ChartEngine({
                container:dom,
                preferences: {
                    "currentPriceLine": true,
                    "whitespace": 0
                },
                layout:{
                    "crosshair": true,
                    chartType:options.chartType || 'mountain'
                }
            });
        }

            //使用feed文件获取数据
        window.quoteFeedSimulator.initial(jsonList);
        this.stxx.attachQuoteFeed(window.quoteFeedSimulator, { refreshInterval: 1 });

        this.timeUnit = options.timeUnit || 'minute';
        this.interval = options.interval || 1;
        this.period = options.interval || 1

        this.stxx.newChart(
            this.contract, 
            null,
            null,
            ()=>{
                if (options.tradeVolume) {
                    window.CIQ.Studies.addStudy(
                        this.stxx, 
                        "vol undr", 
                        {}, 
                        {
                            "Up Volume": options.tradeVolume.upVolume || "red",
                            "Down Volume": options.tradeVolume.downVolume || "green",
                            "Up Border": options.tradeVolume.upBorder || "gray",
                            "Down Border": options.tradeVolume.downBorder || "gray",
                        }
                    );
                }
            },
            {periodicity:{period:this.period,timeUnit:this.timeUnit,interval:this.interval}}
        ); 
        // this.stxx.chart.yAxis.width = 50;
        // this.stxx.addEventListener('newChart',({stx,symbol,symbolObject,moreAvailable,quoteDriver})=>{

		// 	console.log('TCL: init -> quoteDriver', quoteDriver)
		// 	console.log('TCL: init -> moreAvailable', moreAvailable)
		// 	console.log('TCL: init -> symbolObject', symbolObject)
		// 	console.log('TCL: init -> symbol', symbol)
        //     console.log('TCL: init -> stx', stx)
            
        //     if (this.interval !== stx.layout.interval || this.timeUnit !== stx.layout.timeUnit) {
        //         if (this.interval === 1 && this.timeUnit === 'minutes' && this.type === 'slice') {
        //             console.log('mountain');
        //             this.swapChartStyle('mountain');
        //         } else {
        //             console.log('candle');
        //             this.swapChartStyle('candle');
        //         }
        //     }
            
        // });
        this.initial = true;


        
    //    window.CIQ.ChartEngine.prototype.prepend('headsUpHR', this.fillInHUD);
    },

    fillInHUD () {
        var priorBar=null;
        var bar=this.stxx.barFromPixel(this.stxx.cx);
        if(bar===priorBar) return;
        priorBar=bar;
        var barData=this.stxx.chart.xaxis[bar];
        if(barData==null){
            priorBar=-1;
        } else {
            var barHigh = barData.data.High;
            //assumes High is returned from your quotefeed.
           if(barHigh){
               //replace dom with updated high.
                 document.querySelector('#hud-high').innerHTML = barHigh;
           }else{
                   //make DOM empty.
                document.querySelector('#hud-high').innerHTML = '';
           }
       }
    },

    /**销毁 */
    destroy () {
        this.stxx.clear();
        this.stxx.destroy();
        this.stxx = null;
        this.contract = null;
        this.initial = false;
        this.timeUnit = null;
        this.interval = null;
        this.period = null;
        this.type = 'slice';
        let dom = document.querySelector(this.domName);
        while (dom && dom.firstChild) {
            if (dom && dom.firstChild) {
                dom && dom.removeChild(dom && dom.firstChild);
            }
        }
        
        this.domName = null;
    },

    /**更换行情
     * @param {String} contract
     */
    swapContract(contract){
        if (!contract) return console.error('缺少初始商品ID');
        this.contract = contract
        this.stxx.newChart(this.contract,null,null,null)
    },

    /**更换分辨率
     * 
     */
    swapResolution({period,timeUnit,interval},type='dynamic'){
        this.type = type;
        this.stxx.newChart(
            this.contract, 
            null,
            null,
            null,
            {periodicity:{period:period,timeUnit:timeUnit,interval:interval}}
        ); 
    },

    /**可以设置一堆参数
     * 
     */
    updateChart({masterData=null,chart=null,cb=null,option}){
        this.stxx.newChart(
            this.contract, 
            masterData,
            chart,
            cb,
            option
        ); 
    },

    /**更换行情图样式
     * 以下是一些样式
     * "none"
        "line"
        "step"
        "mountain"
        "baseline_delta"
        "candle"
        "bar"
        "hlc"
        "wave"
        "scatterplot"
        "histogram"
        "rangechannel"
     */
    swapChartStyle(style){
        this.stxx.layout.chartType = style;
        this.stxx.draw();
    },
    /**更换行情图样式与分辨率
     * 以下是一些样式
     * "none"
        "line"
        "step"
        "mountain"
        "baseline_delta"
        "candle"
        "bar"
        "hlc"
        "wave"
        "scatterplot"
        "histogram"
        "rangechannel"
     */
    swapResolutionAndChartStyle({period,timeUnit,interval},style){
        this.swapChartStyle(style);
        this.swapResolution({period,timeUnit,interval});
    }
} 

export{
    ChartIQ
}