import React,{Component} from 'react'
import './iqChart.scss'
//时间参数
let buttons = [
    {
        name:'分时',
        value:'',
        timeUnit:'minute',
        interval:1
    },
    {
        name:'日线',
        value:'',
        timeUnit:'day',
        interval:1
    },
    {
        name:'1分',
        value:'',
        timeUnit:'minute',
        interval:1
    },
    {
        name:'3分',
        value:'',
        timeUnit:'minute',
        interval:3
    },
    {
        name:'5分',
        value:'',
        timeUnit:'minute',
        interval:5
    },
    {
        name:'15分',
        value:'',
        timeUnit:'minute',
        interval:15
    }
]

export default class IQChart extends Component{

    // chart对象
    stxx = null;
    //时间单位
    timeUnit = 'minute';
    //间隔
    constructor(props){
        super(props);

        this.state = {
            selectedIndex:0
        }
    }

    componentDidMount(){
        if (!!this.props.symbol) {
            this.updateChart(this.props.symbol);
        }
    }

    componentWillReceiveProps(nextProps){
        if ( !!nextProps.symbol&&(nextProps.symbol !== this.props.symbol)) {
			console.log('TCL: IQChart -> componentWillReceiveProps -> nextProps.symbol', nextProps.symbol)
            //改变k线图
            this.updateChart(nextProps.symbol);
            // this.stxx.newChart(
            //     nextProps.symbol, 
            //     null,
            //     null,
            //     null,
            //     {periodicity:{period:1,timeUnit:this.timeUnit,interval:this.interval}}
            // ); 
        }else{
            return false;
        }
    }

    renderButtons(){
        return buttons.map(({name,timeUnit,interval},i)=>{
            return(
                <li key={i}>
                    <div className={this.state.selectedIndex===i?'active':''} onClick={()=>{this.selectType({timeUnit:timeUnit,interval:interval},i)}}>{name}</div>
                </li>
            )
        })
    }
    
    render(){
        return(
            <div className={'myChartBox'} style={{display:'flex',flex:1,flexDirection:'column'}}>
                <ul>
                    {this.renderButtons()}
                </ul>
                {/* <div className={'info'}></div> */}
                <div className={'chart'}></div>
            </div>
        );
    }

    //初始化或更新图表
    updateChart(symbol){

        // var symbol = this.props.symbol;
        if (symbol === undefined) {
            return;
        }

        if (this.stxx === null) {
            this.stxx=new window.CIQ.ChartEngine({
                container:document.querySelector(".chart"),
                preferences: {
                    "currentPriceLine": true,
                    "whitespace": 0
                },
                layout:{
                    "crosshair": true,
                    chartType:'mountain'
                }
            });
            this.stxx.setStyle('stx-zoom-out','width',30);
            this.stxx.draw();
            //使用feed文件获取数据
            this.stxx.attachQuoteFeed(window.quoteFeedSimulator, { refreshInterval: 1 });
			console.log('TCL: updateChart -> window.quoteFeedSimulator', this.stxx)
            // new window.CIQ.Tooltip({stx:this.stxx, ohl:true});
        }
        
        

        // let dtRight = new Date();
        // let dtLeft = (dtRight.getTime()/1000) - 600000//that.periodLengthSeconds(resolution, 10);
        // dtLeft = new Date(dtLeft);

        this.stxx.newChart(
            symbol, 
            null,
            null,
            null,
            {periodicity:{period:1,timeUnit:this.timeUnit,interval:this.interval}},
            {fillGaps: true,useAsLastSale: true, secondarySeries: "IBM"}
        ); 
    }

    //选择k线图类型
    selectType(obj,index){
        this.timeUnit = obj.timeUnit;
        this.interval = obj.interval
        if (this.timeUnit === 'minute' && this.interval === 1) {
            this.stxx.setChartType('mountain');
        } else {
            this.stxx.setChartType('candle');
        }
        this.stxx.setPeriodicity({period:1, timeUnit:this.timeUnit,interval:this.interval});

        this.setState({selectedIndex:index});
    }

}