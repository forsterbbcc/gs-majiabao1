import {Req} from "../network/Req";
import {exposure} from "../../core/store";
import {TEST} from "../../lib/data/test";
import ERROR from './error';
import Account from './account'

export default {
    getVerifyCode() {
        return `/api/vf/verifyCode.jpg?_=${new Date()}`;
    },
    confirmCode(mobile = '0', imageCode = '0000', test = true) {
        if (test) {
            if (!TEST.PHONE.test(mobile)) return ERROR.PROMISE('请输入的正确的手机号码');
            if (!TEST.CAPTCHA.test(imageCode)) return ERROR.PROMISE('请输入正确的图片验证码')
        }
        return Req({
            url: '/api/sso/register.htm',
            type: 'POST',
            data: {
                action: 'sendCode',
                mobile: mobile,
                imageCode: imageCode
            },
            animate: true
        })
    },
    verify(verifyCode = '0000', test = true) {
        if (test) {
            if (!TEST.CAPTCHA.test(verifyCode)) return ERROR.PROMISE('请输入正确的手机验证码');
        }
        return Req({
            url: '/api/sso/register.htm',
            type: 'POST',
            data: {
                action: 'verifyCode',
                verifyCode: verifyCode
            },
            animate: true
        })
    },
    username: '',
    submit(username = '', password = '', test = true) {
        if (test) {
            if (!TEST.NICKNAME.test(username)) return ERROR.PROMISE('请输入正确的昵称');
            if (!TEST.PASSWORD.test(password)) return ERROR.PROMISE('密码格式不正确');
        }

        this.username = username;
        return Req({
            url: '/api/sso/register.htm',
            type: 'POST',
            data: {
                action: 'register',
                username: username,
                password: password
            },
            animate: true
        });
    },
    callback() {
        localStorage.setItem('isLogin', 'true');
        localStorage.setItem('mobile', this.username);
        Account.isLogin = true;
        Account._keepUpdate(true,()=>{
            exposure('loginCallback');
        });
    }
};