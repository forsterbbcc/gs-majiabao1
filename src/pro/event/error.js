let Alert = null;
export default {
    throw(err){
        let msg = err;
        if(typeof err === 'object' && err !== null){
            msg = err.errorMsg || err.resultMsg || err;
            console.warn(err);
        }
        if(err === null|| err === '请求超时'){
            console.warn('请求超时');
        }else{
            if(Alert){
                Alert(msg);
            }else{
                console.warn(msg)
            }
        }

    },
    setAlert(func){
        if(typeof func === 'function'){
            Alert = func;
        }
    },
    PROMISE(msg){
        return new Promise((resolve,reject)=>{
            reject(msg)
        })
    }
}