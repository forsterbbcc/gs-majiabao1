import {Req} from "../network/Req";
import {
    formatDate, getCloseTime, getIdentity, getLocalStore, getPlatform, insertLocalStore,
    setLocalStore
} from "../../lib/utils";
import {exposure} from "../../core/store";
import ERROR from './error';
import Account from './account';


let tidesForOrderConfig = null;

function bridgeForOrderConfig(wakeUp) {
    tidesForOrderConfig = wakeUp
}

let tidesForTrade = null;

function bridgeForTrade(wakeUp) {
    tidesForTrade = wakeUp
}

/**
 * @constructor
 * 订单操作
 */
class OrderConfig {

    _callback = null;
    constructor(order, simulate) {
        this.id = order.id;
        this.name = order.commodity;
        this.contract = order.contract;
        this.priceDigit = order.priceDigit;
        this.des = order.des;
        this.isBuy = order.isBuy;
        this.class = order.isBuy ? 'raise' : 'fall';
        this.moneyType = order.moneyType;
        this.simulate = simulate;
        this.income = order.income;
        this.open = order.opPrice;
        this.current = order.current;
        this.volume = order.opVolume;
        this.charge = order.charge;
        this.priceChange = order.priceChange;
        this.unit = order.unit;

        /**
         * 当前止损金额
         * @type {*|number}
         */
        this.stopLoss = order.stopLoss;
        /**
         * 最小止损金额(仅显示)
         * @type {number}
         */
        this.stopLossMin = -order.unit.mul(order.opVolume);
        /**
         * 最大止损金额(仅显示)
         */
        this.stopLossMax = order.stopLossBegin;
        /**
         * 止损调整最大Step
         * @type {Number}
         */
        this.stopLossStep = Math.abs(order.stopLossBegin).div(order.unit.mul(order.opVolume));
        /**
         * 止损的当前Step
         * @type {Number}
         */
        this.stopLossIndex = this.stopLoss.div(this.stopLossMin);
        /**
         * 当前止盈金额
         * @type {*|number}
         */
        this.stopProfit = order.stopProfit;
        /**
         * 最小止盈金额(仅显示)
         * @type {number}
         */
        this.stopProfitMin = order.unit.mul(order.opVolume);
        /**
         * 最大止盈金额(仅显示)
         */
        this.stopProfitMax = order.stopProfitBegin;
        /**
         * 止盈调整最大Step
         * @type {Number}
         */
        this.stopProfitStep = order.stopProfitBegin.div(order.unit.mul(order.opVolume));
        /**
         * 止盈的当前Step
         * @type {Number}
         */
        this.stopProfitIndex = this.stopProfit.div(this.stopProfitMin);


        let gapForLoss = this.stopLossIndex.mul(this.priceChange);
        let maxForLoss = this.stopLossStep.mul(this.priceChange);
        let gapForProfit = this.stopProfitIndex.mul(this.priceChange);
        let maxForProfit = this.stopProfitStep.mul(this.priceChange);
        this.stopLossPrice = this.open.add(this.isBuy ? -gapForLoss : gapForLoss);
        this.stopLossMinPrice = this.open.add(this.isBuy ? -this.priceChange : this.priceChange);
        this.stopLossMaxPrice = this.open.add(this.isBuy ? -maxForLoss : maxForLoss);
        this.stopProfitPrice = this.open.add(this.isBuy ? gapForProfit : -gapForProfit);
        this.stopProfitMinPrice = this.open.add(this.isBuy ? this.priceChange : -this.priceChange);
        this.stopProfitMaxPrice = this.open.add(this.isBuy ? maxForProfit : -maxForProfit);
        bridgeForOrderConfig(this._wakeUp.bind(this))
    }

    _wakeUp(store) {
        if (!!store[this.contract]) {
            this.current = Number(store[this.contract].price);
            if (this.isBuy) {
                this.income = this.current.sub(this.open).div(this.priceChange).mul(this.volume).mul(this.unit);
            } else {
                this.income = this.open.sub(this.current).div(this.priceChange).mul(this.volume).mul(this.unit);
            }
            this._callback && this._callback();
        }
    }

    whileUpdated(callback) {
        if (callback) this._callback = callback;
    }

    setStopLoss(index = 1) {
        if (index > this.stopLossStep) index = this.stopLossStep;
        const gap = index.mul(this.priceChange);
        this.stopLoss = index.mul(this.stopLossMin);
        this.stopLossPrice = this.open.add(this.isBuy ? -gap : gap);
        this.stopLossIndex = index;
    }

    setStopProfit(index) {
        if (index > this.stopProfitStep) index = this.stopProfitStep;
        const gap = index.mul(this.priceChange);
        this.stopProfit = index.mul(this.stopProfitMin);
        this.stopProfitPrice = this.open.add(this.isBuy ? gap : -gap);
        this.stopProfitIndex = index;
    }

    submit() {
        if(this.income > this.stopProfit) return ERROR.PROMISE('止盈金额不能低于当前已盈利金额');
        return Req({
            url: '/api/trade/spsl.htm',
            type: 'POST',
            data: {
                bettingId: this.id,
                tradeType: this.simulate ? 2 : 1,
                stopProfit: this.stopProfit,
                stopLoss: this.stopLoss,
                source: '设置止盈止损'
            },
            animate: true
        });
    }
}

/**
 * @constructor
 * 持仓对象
 */
class Position {
    _control = {};
    _store = {};
    _storeList = [];
    _sim_store = {};
    _sim_storeList = [];
    simulate = true;
    _keepUpdate = false;
    _rate = [1, 0.1, 0.01];
    _isBuy = '买涨:买跌';
    _callback = null;
    _identity = null;

    constructor(data, options) {
        const def = {
            simulate: false,
            isBuy: '买涨:买跌'
        };
        options = Object.assign(def, options);
        if (options.isBuy.indexOf(':') !== -1) this._isBuy = options.isBuy;
        if (options.callback !== undefined) this._callback = options.callback;
        this._control = data;
        this.simulate = options.simulate;
        this._keepUpdate = true;
        this._identity = getIdentity(12);
        insertLocalStore('position', this._identity);
        this._update().catch(() => {
        });
    }

    async _update() {
        try {
            const p = getLocalStore('position', 'Array');
            if (p.length > 1 && p[0] === this._identity) {
                return this.destroy();
            }
            let {data} = await Req({
                url: '/api/trade/scheme.htm',
                data: {
                    schemeSort: 1,
                    tradeType: this.simulate ? 2 : 1,
                    beginTime: '',
                    _: new Date().getTime(),
                }
            });
            if (data) {
                this._process(data, this.simulate);
                this._live();
            }
        } catch (err) {
            ERROR.throw(err);
        } finally {
            if (this._keepUpdate !== null) {
                this._keepUpdate = setTimeout(() => this._update(), 1000);
            }
        }
    }

    _live() {
        exposure('positionUpdate');
        if (this._callback)
            this._callback();
    }

    _process(data, simulate) {
        let control;
        let income = 0;
        const [desBuy, desSell] = this._isBuy.split(':');
        let position = data.map((e) => {
            control = this._control[e.contract];
            if (control) {
                const base = control.chargeUnitList[0];
                const rate = this._rate[e.moneyType];
                e.unit = control.priceUnit.mul(rate);
                e.current = Number(control.price) || 0;
                e.charge = base.mul(e.volume).mul(rate);
                e.priceChange = control.priceChange;
                if (control.price !== '0.00' && !!e.opPrice) {
                    if (e.isBuy) {
                        e.des = desBuy;
                        e.income = e.current.sub(e.opPrice).div(control.priceChange).mul(e.volume).mul(e.unit);
                    } else {
                        e.des = desSell;
                        e.income = e.opPrice.sub(e.current).div(control.priceChange).mul(e.volume).mul(e.unit);
                    }
                    income = income.add(e.income);
                    return e;
                } else {
                    e.income = 0;
                    return e;
                }
            } else {
                return null;
            }
        });
        position.remove(null);
        this._insert(position, simulate);
    }

    _insert(data, simulate) {
        const target = simulate ? this._sim_store : this._store;
        const list = simulate ? this._sim_storeList : this._storeList;
        const compare = [];
        data.forEach((o) => {
            target[o.id] = o;
            compare.push(o.id)
        });
        const close = list.differ(compare);
        this._closeNotice(close, target, simulate);
        list.insert(compare);
    }

    /**
     * 进行平仓数据处理
     */
    _closeNotice(close, target, simulate) {
        const list = simulate ? this._sim_storeList : this._storeList;
        close.forEach((e) => {
            delete target[e];
            list.remove(e);
        });
        if(close.length > 0){
            //todo 使用close进行广播
            exposure('positionClose', close)
        }
    }

    /**
     * 订单搜索
     * @param id
     * @returns {*}
     * @private
     */
    _searching(id) {
        if (this._store[id]) return [this._store[id], false];
        if (this._sim_store[id]) return [this._sim_store[id], true];
        return [null, null];
    }

    destroy() {
        clearTimeout(this._keepUpdate);
        this._keepUpdate = null;
        this._callback = null;
        const o = getLocalStore('position', 'Array');
        o.remove(this._identity);
        setLocalStore('position', o, 'Array');
    }

    /**
     * 切换实盘及模拟盘持仓更新
     * @param simulate
     */
    swapSimulate(simulate = false) {
        this.simulate = simulate;
    }

    /**
     * 初级的持仓获取
     * @deprecated
     * @param simulate
     * @returns {*}
     */
    getStore(simulate = false) {
        const targetList = simulate ? this._sim_storeList : this._storeList;
        const target = simulate ? this._sim_store : this._store;
        return targetList.map((e) => {
            return target[e]
        })
    }

    /**
     * 获取持仓商品
     * @param simulate
     * @returns {{total: number, data: any[]}}
     */
    getStatistics(simulate = false) {
        const targetList = simulate ? this._sim_storeList : this._storeList;
        const target = simulate ? this._sim_store : this._store;
        let total = 0;
        const data = targetList.map((e) => {
            total = total.add(target[e].income);
            return target[e]
        });
        return {total, data};
    }

    /**
     * 根据合约号获取持仓商品
     * @param contract
     * @param simulate
     * @returns {*}
     */
    getStatisticsByContract(contract, simulate = false) {
        if (!contract) return this.getStatistics(simulate);
        const target = simulate ? this._sim_store : this._store;
        const data = [];
        let total = 0;
        for (let [, val] of Object.entries(target)) {
            if (val.contract === contract) {
                total = total.add(val.income);
                data.push(val)
            }
        }
        return {total, data};
    }

    /**
     * 平仓
     * @param id
     * @returns {Promise<any>}
     */
    close(id) {
        return new Promise(async (resolve, reject) => {
            try {
                const [, simulate] = this._searching(id);
                if (simulate === null) return reject('订单号不存在');
                await Req({
                    url: '/api/trade/close.htm',
                    type: 'POST',
                    data: {
                        bettingId: id,
                        tradeType: simulate ? 2 : 1,
                        source: '下单'
                    },
                    animate: true
                });
                resolve();
                Account.getFinanceUserInfo();
            } catch (error) {
                reject(error)
            }
        });
    }

    /**
     * 全部平仓
     * @param simulate
     * @returns {Promise<any>}
     */
    closeAll(simulate = false) {
        return new Promise(async (resolve, reject) => {
            try {
                let list = simulate ? this._sim_storeList : this._storeList;
                list = list.join(',');
                const result = Req({
                    url: '/api/trade/close.htm',
                    type: 'POST',
                    data: {
                        bettingList: list,
                        tradeType: simulate ? 2 : 1,
                        source: '下单'
                    },
                    animate: true
                });
                resolve(result);
                Account.getFinanceUserInfo();
            } catch (error) {
                reject(error)
            }
        });

    }

    /**
     * 指定商品全部平仓
     * @param contract
     * @param simulate
     * @returns {Promise<any>}
     */
    closeByContract(contract, simulate = false) {
        return new Promise(async (resolve, reject) => {
            try {
                if (!contract) return reject('请传入合约号');
                const target = simulate ? this._sim_store : this._store;
                let list = simulate ? this._sim_storeList : this._storeList;
                list = list.filter((e) => {
                    return target[e].contract === contract
                });
                list = list.join(',');
                const result = Req({
                    url: '/api/trade/close.htm',
                    type: 'POST',
                    data: {
                        bettingList: list,
                        tradeType: simulate ? 2 : 1,
                        source: '下单'
                    },
                    animate: true
                });
                resolve(result.successNumber, result.failNumber);
                Account.getFinanceUserInfo();
            } catch (error) {
                reject(error)
            }
        })
    }

    /**
     * 反买
     * @param id
     */
    reverseBuy(id) {
        return new Promise(async (resolve, reject) => {
            try {
                const [order, simulate] = this._searching(id);
                if (order === null) return reject('订单不存在或已平仓');
                const control = this._control[order.contract];
                const base = control.chargeUnitList[0];
                await Req({
                    url: '/api/trade/close.htm',
                    type: 'POST',
                    data: {
                        bettingId: id,
                        tradeType: simulate ? 2 : 1,
                        source: '反向'
                    },
                    animate: true
                });
                await Req({
                    url: '/api/trade/open.htm',
                    type: 'POST',
                    data: {
                        identity: getIdentity(16),
                        tradeType: simulate ? 2 : 1,//模拟交易2 实盘交易1
                        source: '反向',  // 买入来源（下单、反向、快捷)
                        commodity: control.code,
                        contract: order.contract,
                        isBuy: !order.isBuy,
                        price: 0,
                        stopProfit: order.stopProfitBegin,
                        stopLoss: order.stopLossBegin,
                        serviceCharge: base.mul(order.volume).mul(this._rate[order.moneyType]),
                        eagleDeduction: 0,
                        volume: order.volume,
                        moneyType: order.moneyType,
                        platform: getPlatform()
                    },
                    animate: true,
                    timeout: 30
                });
                Account.getBasicUserInfo();
                Account.getFinanceUserInfo();
                resolve()
            } catch (error) {
                reject(error);
            }
        })


    }

    /**
     * 追单
     * @param id
     */
    addBuy(id) {
        return new Promise(async (resolve, reject) => {
            try {
                const [order, simulate] = this._searching(id);
                if (order === null) return reject('订单不存在或已平仓');
                const control = this._control[order.contract];
                const base = control.chargeUnitList[0];
                await Req({
                    url: '/api/trade/open.htm',
                    type: 'POST',
                    data: {
                        identity: getIdentity(16),
                        tradeType: simulate ? 2 : 1,//模拟交易2 实盘交易1
                        source: '追单',  // 买入来源（下单、反向、快捷)
                        commodity: control.code,
                        contract: order.contract,
                        isBuy: order.isBuy,
                        price: 0,
                        stopProfit: order.stopProfitBegin,
                        stopLoss: order.stopLossBegin,
                        serviceCharge: base.mul(order.volume).mul(this._rate[order.moneyType]),
                        eagleDeduction: 0,
                        volume: order.volume,
                        moneyType: order.moneyType,
                        platform: getPlatform()
                    },
                    animate: true,
                    timeout: 30
                });
                Account.getBasicUserInfo();
                Account.getFinanceUserInfo();
                resolve()
            } catch (error) {
                reject(error);
            }
        });
    }

    /**
     * 获取订单操作对象
     * @param id
     * @returns {*}
     */
    getOrderConfig(id) {
        const [order, simulate] = this._searching(id);
        if (order === null) return null;
        return new OrderConfig(order, simulate);
    }
}

//todo 增加积分抵扣方法及相关参数
/**
 * @constructor
 * 交易模型
 */
class Trade {
    origin = {};

    priceChange = 0;
    priceUnit = 0;

    rateList = [1, 0.1, 0.01];
    rateName = ['元', '角', '分'];
    rate = 1;
    moneyType = 0;
    moneyTypeList = [];

    charge = 0;
    chargeList = [];
    chargeSpec = false;

    select = 0;

    stopLoss = 0;
    stopLossList = [];

    stopProfit = 0;
    stopProfitList = [];

    volumeIndex = 0;
    volume = 1;
    volumeList = [];

    simulate = false;

    /**
     * 积分相关
     * @type {boolean}
     */
    useEagle = false;
    usableEagle = true;
    deductMoney = 0;
    deductEagle = 0;
    deductCharge = 0;

    userInfo = {
        money: 0,
        game: 0,
        eagle: 0,
        eagleRatio: 10,
        eagleMax: 1,
        partDeduct: false
    };

    /**
     * 保留区
     */
    closeTime = '00:00:00';

    _callback = null;

    constructor(origin, userInfo, options = {simulate: false}) {
        this.origin = origin;
        this.code = origin.contract;
        this.name = origin.name;
        this.simulate = options.simulate;
        if (origin.moneyType > 0) {
            for (let i = 0; i <= origin.moneyType; i++) {
                this.moneyTypeList.push(i)
            }
        } else {
            this.moneyTypeList.push(0)
        }
        const l = this.moneyTypeList.length;
        if (l > 1) {
            this.rateName = this.rateName.slice(0, l)
        } else {
            this.rateName = ['元']
        }
        this.priceChange = origin.priceChange;
        this.priceUnit = origin.priceUnit;

        this.stopLossList = origin.stopLossList;
        this.stopLoss = this.stopLossList[0];

        this.stopProfitList = origin.stopProfitList;
        this.stopProfit = this.stopProfitList[0];

        this.volumeList = origin.volumeList;
        this.volume = this.volumeList[0];

        this.chargeList = origin.chargeUnitList;
        this.charge = this.chargeList[0];
        if (this.chargeList.length > 1) {
            this.chargeSpec = true;
        }

        this.closeTime = origin.closeTime;

        for (let o of Object.keys(userInfo)) {
            this.userInfo[o] = userInfo[o]
        }

        bridgeForTrade(this._wakeUp.bind(this));
    }

    _wakeUp(data) {
        for (let o of Object.keys(data)) {
            this.userInfo[o] = data[o]
        }
        this._callback && this._callback();
    }

    whileUpdated(callback) {
        if (callback) this._callback = callback;
    }

    show(point = false) {
        const money = this.simulate ? this.userInfo.game : this.userInfo.money;
        const moneyName = this.simulate ? ['元'] : this.rateName;
        const moneyTypeList = this.simulate ? [0] : this.moneyTypeList;

        this.calculate();

        return {
            name: this.name,
            code: this.code,
            total: this.charge.add(Math.abs(this.stopLoss)),
            bail: Math.abs(this.stopLoss),
            priceCharge: this.charge,
            simulate: this.simulate,
            select: this.select,
            stopLoss: this.stopLoss,
            stopLossList: this.stopLossList,
            stopProfit: this.stopProfit,
            stopProfitList: this.stopProfitList,
            volumeIndex: this.volumeIndex,
            volumeList: this.volumeList,
            volume: this.volume,
            moneyType: this.moneyType,
            rateName: this.rateName,
            closeTime: this.closeTime,
            /**
             * 是否使用积分抵扣
             */
            useEagle: this.useEagle,
            /**
             * 是否可用积分抵扣
             */
            usableEagle: this.usableEagle,
            /**
             * 当前积分
             */
            eagle: this.userInfo.eagle,
            /**
             * 最多抵扣多少金额
             */
            maxDeductMoney: this.userInfo.eagle.div(this.userInfo.eagleRatio),
            /**
             * 当前抵扣的金额
             */
            deductMoney: this.deductMoney,
            /**
             * 当前消耗的积分
             */
            deductEagle: this.deductEagle,
            /**
             * 积分抵扣后剩余的手续费
             */
            deductCharge: this.deductCharge,
            /**
             * 原始的手续费
             */
            charge: this.charge,
            moneyTypeList,
            moneyName,
            money,
        }
    }

    swapSimulate(simulate = false) {
        this.simulate = simulate;
        this.moneyType = 0;
        this.rate = 1;
        this.useEagle = false;

        this.calculate();

        return this.show()
    }

    swapMoneyType(index) {
        if (this.simulate) return this.show();

        const max = this.moneyTypeList.length - 1;
        if (index > max) index = max;
        this.moneyType = index;
        this.rate = this.rateList[index];

        this.calculate();

        return this.show()
    }

    swapVolume(index) {
        const max = this.volumeList.length - 1;
        if (index > max) index = max;
        index = Math.max(index, 0);

        this.volumeIndex = index;
        this.volume = this.volumeList[index];

        this.calculate();

        return this.show();
    }

    swapStopLoss(index) {
        const max = this.stopLossList.length - 1;
        if (index > max) index = max;
        index = Math.max(index, 0);

        this.select = index;

        this.calculate();

        return this.show()
    }

    swapEagle(state = false) {
        this.useEagle = state;
        return this.show();
    }

    calculate() {
        this.stopProfitList = this.origin.stopProfitList.map((e) => {
            return e.mul(this.volume).mul(this.rate)
        });
        this.stopProfit = this.stopProfitList[this.select];

        this.stopLossList = this.origin.stopLossList.map((e) => {
            return e.mul(this.volume).mul(this.rate)
        });
        this.stopLoss = this.stopLossList[this.select];

        this.chargeList = this.origin.chargeUnitList.map((e) => {
            return e.mul(this.volume).mul(this.rate)
        });

        if (this.chargeSpec) {
            this.charge = this.chargeList[this.select]
        } else {
            this.charge = this.chargeList[0];
        }

        const available = this.userInfo.eagle.div(this.userInfo.eagleRatio);
        const max = this.charge.mul(this.userInfo.eagleMax);
        this.usableEagle = (available >= max || this.userInfo.partDeduct) && !this.simulate;
        this.usableEagle = available >= max || this.userInfo.partDeduct;

        if (this.usableEagle && this.useEagle) {
            if (max >= available) {
                this.deductEagle = this.userInfo.eagle;
                this.deductMoney = available;
                this.deductCharge = this.charge.sub(available);
            } else {
                this.deductEagle = max.mul(this.userInfo.eagleRatio);
                this.deductMoney = max;
                this.deductCharge = this.charge.sub(max);
            }
        } else {
            this.useEagle = false;
            this.deductEagle = 0;
            this.deductMoney = 0;
            this.deductCharge = this.charge;
        }
    }

    order(isBuy = false) {
        return new Promise(async (resolve, reject) => {
            try {
                const result = await Req({
                    url: '/api/trade/open.htm',
                    type: 'POST',
                    data: {
                        identity: getIdentity(16),
                        tradeType: this.simulate ? 2 : 1,//模拟交易2 实盘交易1
                        source: '下单',  // 买入来源（下单、反向、快捷)
                        commodity: this.origin.code,
                        contract: this.origin.contract,
                        isBuy: isBuy,
                        price: 0,
                        stopProfit: this.stopProfit,
                        stopLoss: this.stopLoss,
                        serviceCharge: this.deductCharge,
                        eagleDeduction: this.deductEagle,
                        volume: this.volume,
                        moneyType: this.moneyType,
                        platform: getPlatform()
                    },
                    animate: true,
                    timeout: 30
                });
                Account.getBasicUserInfo();
                Account.getFinanceUserInfo();
                this.message = result.errorMsg;
                resolve()
            } catch (error) {
                console.log(error);
                reject(error);
            }
        });
    }
}

export default {
    name: 'Trade',
    initial: false,
    _tradeList: null,
    _contracts: null,
    userInfo: {
        money: 0,
        game: 0,
        eagle: 0,
        eagleRatio: 10,
        eagleMax: 1,
        partDeduct: false
    },
    /**
     * 清单
     */
    list: [],
    /**
     * 商品
     */
    commodity: {},
    /**
     * 仓库
     */
    depot: [],
    /**
     * 是否有快速交易
     */
    tradeQuick: false,

    keeper: null,
    librarian: null,
    _insertContracts(data) {
        if (!this.initial) {
            this._contracts = data;
            this._init();
        }
    },
    _insertTradeList(data, tradeQuick = false) {
        if (!this.initial) {
            this._tradeList = data;
            this.tradeQuick = tradeQuick;
            this._init();
        }
    },
    _init() {
        if (this._tradeList !== null && this._contracts !== null) {
            for (let [key, val] of Object.entries(this._contracts)) {
                if (key === '') continue;
                this.list.push(key);
                const {commCode, commName, chargeUnitList, priceDigit, priceChange, priceUnit, stopLossList, stopProfitList, volumeList, spread, closeTime} = this._tradeList[key];
                const {moneyType} = val;
                this.commodity[key] = {
                    name: commName,
                    code: commCode,
                    contract: key,
                    chargeUnitList,
                    priceDigit,
                    priceChange,
                    priceUnit: priceChange.mul(priceUnit),
                    stopLossList,
                    stopProfitList,
                    volumeList,
                    spread,
                    moneyType,
                    closeTime: formatDate('h:i:s', {date: getCloseTime(closeTime)}),
                    isUp: false,
                    isOpen: false,
                    price: '0.00',
                    rate: '0.00%'
                };
            }
            this.initial = true;
            this._contracts = null;
            this._tradeList = null;
            localStorage.removeItem('position');
            exposure('tradeInitial')
        }
    },

    /**
     * 接收行情的后门
     * @param data
     */
    _backdoorForData(data) {
        for (let key of this.list) {
            this.commodity[key].isUp = data[key].isUp;
            this.commodity[key].isOpen = data[key].isOpen;
            this.commodity[key].price = data[key].price;
            this.commodity[key].rate = data[key].rate;
        }
        tidesForOrderConfig && tidesForOrderConfig(this.commodity);
    },

    /**
     * 接收用户数据更新的后门
     * @param data
     * @private
     */
    _backdoorForUser(data) {
        for (let o of Object.keys(this.userInfo)) {
            if (data[o]) this.userInfo[o] = data[o]
        }
        tidesForTrade && tidesForTrade(this.userInfo);
    },

    /**
     * 设置积分抵扣手续费的最大比例
     * @param {Number} ratio
     */
    setMaxEaglePercent(ratio) {
        if (typeof ratio !== "number") return;
        if (ratio > 1 || ratio < 0) return;
        this.userInfo.eagleMax = ratio;
    },

    /**
     * 设置是否允许部分抵扣手续费
     * @param {Boolean} available
     */
    setPartEagleDeduct(available) {
        if (typeof available !== "boolean") return;
        this.userInfo.partDeduct = available;
    },

    /**
     *
     * @param code
     * @param options
     * @returns {Trade|Object}
     * @constructor
     */
    RFQ(code = null, options) {
        // if (!code)
        //     return this.keeper;
        // this.keeper = new Trade(this.commodity[code], this.userInfo, options);
        // return this.keeper;
        if (!this.initial) return {
            show() {
            }, swapSimulate() {
            }
        };
        return new Trade(this.commodity[code], this.userInfo, options);
    },
    /**
     * @param options
     * @returns {Position|Object}
     * @constructor
     */
    INVENTORY(options) {
        if (!this.initial) return {
            getStatistics(){},getStatisticsByContract(){}
        };
        if (!options) return this.librarian;
        if (!!options) this.librarian = new Position(this.commodity, options);
        return this.librarian;
    },
    message: '',
    /**
     * 交易接口
     * @description 下下次更新移除
     * @deprecated
     */
    openPosition({simulate, commodity, contract, isBuy, moneyType, volume, serviceCharge, stopLoss, stopProfit}) {
        return new Promise(async (resolve, reject) => {
            try {
                const result = await Req({
                    url: '/api/trade/open.htm',
                    type: 'POST',
                    data: {
                        identity: getIdentity(16),
                        tradeType: simulate ? 2 : 1,//模拟交易2 实盘交易1
                        source: '下单',  // 买入来源（下单、反向、快捷)
                        commodity: commodity,
                        contract: contract,
                        isBuy: isBuy,
                        price: 0,
                        stopProfit: stopProfit.mul(volume).mul(moneyType === 0 ? 1 : 0.1),
                        stopLoss: stopLoss.mul(volume).mul(moneyType === 0 ? 1 : 0.1),
                        serviceCharge: serviceCharge.mul(volume).mul(moneyType === 0 ? 1 : 0.1),
                        eagleDeduction: 0,
                        volume: volume,
                        moneyType: moneyType,
                        platform: getPlatform()
                    },
                    animate: true
                });
                Account.getBasicUserInfo();
                Account.getFinanceUserInfo();
                this.message = result.errorMsg;
                resolve()
            } catch (error) {
                console.log(error);
                reject(error);
            }
        });
    },
}