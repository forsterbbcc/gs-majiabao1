import {Req} from "../network/Req";
import {exposure} from "../../core/store";
import {TEST} from "../../lib/data/test";
import ERROR from './error';
import {getIdentity, getPlatform} from "../../lib/utils";
import {EVENT} from "./index";

export default {
    name: 'Account',
    initial: false,
    isLogin: false,
    _loop: null,
    mobile: '',
    /**
     *用户基本信息
     */
    basicUserData: {
        username: null,
        id: null,
        money: 0,
        game: 0,
        eagle: 0,
        eagleRatio: 10,
        hello: null,
        unread: null,
        registerTime: null
    },
    getBasicUserData() {
        return this.basicUserData;
    },
    /**
     *用户金融信息
     */
    financeUserData: {
        bankCardCount: 0,
        name: null,
        mobile: null,
        identityNumber: null,
        identityNumberValid: null,
        withdrawPw: null,
        level: null
    },
    getFinanceUserData() {
        return this.financeUserData;
    },
    tradeQuick: null,
    tradeList: null,
    message: '',
    /**
     *银行卡列表
     */
    allBankCardList: [],
    getAllBankCardList(antDesign = false) {
        return this.allBankCardList;
    },
    /**
     *充值提款记录
     */
    transferRecordList: [],
    /**
     * 交易明细记录
     */
    transactionRecordList: [],
    /**
     * 推广佣金记录列表
     */
    commissionRecordList: [],
    /**
     * 积分明细列表
     */
    eagleRecordList: [],
    /**
     * 资金明细详细信息
     */
    fundDetailData: null,
    /**提款记录 */
    withdrawRecord:[],
    /**
     * 返回提款记录
     */
    getWithdrawRecord () {
        return this.withdrawRecord;
    },
    /**
     * 推广信息
     */
    union: {
        unionTotal: 0,
        commRatio: 0,
        userCount: 0,
        userConsume: 0,
        visitCount: 0,
        unionVolume: 0,
        linkAddress: ''
    },
    getUnion() {
        return this.union;
    },
    validPhone: null,
    userList: [],
    /**
     * 获取用户列表
     */
    getUserList() {
        return this.userList;
    },
    /**签到历史对象 */
    checkInHistory: {
        award: [], status: [], count: 0
    },
    /**返回历史对象 */
    getCheckInHistory() {
        return this.checkInHistory;
    },
    /**我的交易 需要的数据 */
    heroObj : {
        best:null,
        historyList:[],
        invest:null
    },
    /**返回我的交易 需要的数据 */
    getHero(){
        return this.heroObj;
    },
    async init() {
        try {
            const isLogin = localStorage.getItem('isLogin');
            if (isLogin !== 'true')
                throw '';
            let result = await Req({
                url: '/api/trade/scheme.htm',
                data: {
                    schemeSort: 0,
                    tradeType: 1,
                    beginTime: '',
                    _: new Date().getTime()
                }
            });
            if (result.isLogin) {
                this.mobile = localStorage.getItem('mobile');
                this.isLogin = true;
                await this._keepUpdate(true);
                await this.getUserScheme(result);
            } else {
                localStorage.removeItem('isLogin');
                localStorage.removeItem('mobile')
            }
        } catch (e) {

        } finally {
            this.initial = true;
            exposure('cacheInitial')
        }
    },
    /**
     * @description 持续更新用户数据
     * @param {boolean} [update]
     * @param {function} [callback]
     * @returns {Promise<void>}
     * @private
     */
    async _keepUpdate(update = false,callback) {
        try {
             const [{
                eagleRatio = 10,
                hello,
                unread,
                registerTime,
                user: {username, id},
                asset: {money, game, eagle}
            }, {
                bankCardCount,
                user: {withdrawPw},
                level,
                info: {identityNumberValid, identityNumber, mobile, name}
            }] = await Promise.all([Req({
                url: '/api/mine/index.htm',
                type: 'GET',
                timeout: 10
            }), Req({
                url: '/api/mine/profile.htm',
                type: 'GET',
                timeout: 10
            })]);
            this.insertBasicUserInfo({eagleRatio, hello, unread, registerTime, username, id, money, game, eagle});
            this.insertFinanceUserInfo({
                bankCardCount,
                withdrawPw,
                level,
                identityNumber,
                identityNumberValid,
                mobile,
                name
            });
        } catch (err) {

        } finally {
            exposure('getUserInfo');
            EVENT.Trade._backdoorForUser(this.basicUserData);
            if (update) {
                if(callback) callback();
                this._loop = setTimeout(() => this._keepUpdate(true), 8000)
            }
        }
    },
    setLogout() {
        this.isLogin = false;
        localStorage.removeItem('isLogin');
        localStorage.removeItem('mobile');
        clearTimeout(this._loop);
        exposure('loginCallback')
    },
    /**
     * 获取用户基础信息
     * @descriptions
     */
    getBasicUserInfo() {
        Req({
            url: '/api/mine/index.htm',
            type: 'GET',
        }).then((result) => {
            this.insertBasicUserInfo(result);
            EVENT.Trade._backdoorForUser(this.basicUserData);
            exposure('getBasicUserInfo');
        })
    },
    insertBasicUserInfo(attr) {
        if (!!attr.asset) {
            const {eagleRatio = 10, hello, unread, registerTime, user: {username, id}, asset: {money, game, eagle}} = attr;
            this.insertBasicUserInfo({eagleRatio, hello, unread, registerTime, username, id, money, game, eagle});
        } else {
            for (let [key, val] of Object.entries(attr)) {
                this.basicUserData[key] = val;
            }
        }
    },
    /**
     * 获取用户资产信息
     * @description
     */
    getFinanceUserInfo() {
        Req({
            url: '/api/mine/profile.htm',
            type: 'GET',
        }).then(result => {
            this.insertFinanceUserInfo(result);
            exposure('getFinanceUserInfo');
        })
    },
    insertFinanceUserInfo(attr) {
        if (!!attr.info) {
            const {bankCardCount, user: {withdrawPw}, level, info: {identityNumberValid, identityNumber, mobile, name}} = attr;
            this.insertBasicUserInfo({
                bankCardCount,
                withdrawPw,
                level,
                identityNumber,
                identityNumberValid,
                mobile,
                name
            })
        } else {
            for (let [key, val] of Object.entries(attr)) {
                this.financeUserData[key] = val
            }
        }
    },
    /**
     * 添加模拟余额接口
     */
    addSimulateBalance() {
        return new Promise(async (resolve, reject) => {
            try {
                const result = await Req({
                    url: '/api/trade/addScore.htm',
                    animate: true
                });
                EVENT.Account.getBasicUserInfo();
                this.message = result.resultMsg;
                resolve()
            } catch (error) {
                reject(error);
            }
        })
    },
    /**
     * 获取用户交易规则
     * @param data
     */
    async getUserScheme(data) {
        try {
            if (!data) {
                data = await Req({
                    url: '/api/trade/scheme.htm',
                    data: {
                        schemeSort: 0,
                        tradeType: 1,
                        beginTime: '',
                        _: new Date().getTime()
                    }
                })
            }
            this.tradeQuick = data.tradeQuick;
            if (data.tradeList.length > 0 && this.tradeList === null) {
                this.tradeList = {};
                for (let o of data.tradeList) {
                    this.tradeList[o.contCode] = o;
                }
                EVENT.Trade._insertTradeList(this.tradeList, data.tradeQuick);
            }
            exposure('getUserScheme');
        } catch (err) {
        }
    },
    submit(mobile = '', password = '', test = true) {
        if (test) {
            if (!TEST.PHONE.test(mobile)) return ERROR.PROMISE('手机号码格式错误');
            if (!TEST.PASSWORD.test(password)) return ERROR.PROMISE('密码格式错误');
        }
        this.mobile = mobile;
        return Req({
            url: '/api/sso/user_login_check',
            type: 'POST',
            data: {
                mobile: mobile,
                password: password
            },
            animate: true
        });
    },
    async callback() {
        try {
            localStorage.setItem('isLogin', 'true');
            localStorage.setItem('mobile', this.mobile);
            this.isLogin = true;
            await this._keepUpdate(true);
            await this.getUserScheme()
        } catch (err) {

        } finally {
            exposure('loginCallback');
        }
    },
    async changePassword(oldPass, newPass, comPass) {

        if (oldPass === '') return ERROR.PROMISE('请输入旧密码');
        if (newPass === '') return ERROR.PROMISE('请输入新密码');
        if (comPass === '') return ERROR.PROMISE('请输入确认密码');
        if (newPass !== comPass) return ERROR.PROMISE('两次输入密码不相同');

        return new Promise(async (resolve, reject) => {
            try {
                let result = await Req({
                    url: '/api/mine/loginPasswd.htm',
                    type: 'POST',
                    data: {
                        oldPass: oldPass,
                        newPass: newPass,
                        newPassCfm: comPass
                    },
                    animate: true
                });
                this.message = result.errorMsg;
                resolve()
            } catch (error) {
                reject(error)
            }
        })
    },
    changeWithdrawPassword(oldPass, newPass, comPass) {

        if (oldPass === '') return ERROR.PROMISE('请输入旧密码');
        if (newPass === '') return ERROR.PROMISE('请输入新密码');
        if (comPass === '') return ERROR.PROMISE('请输入确认密码');
        if (newPass !== comPass) return ERROR.PROMISE('两次输入密码不相同');

        return new Promise(async (resolve, reject) => {
            try {

                let result = await Req({
                    url: '/api/mine/atmPasswd.htm',
                    type: 'POST',
                    data: {
                        password: oldPass,
                        withdrawPw: newPass,
                        withdrawPwCfm: comPass
                    }
                })
                this.message = result.errorMsg;
                resolve();
            } catch (error) {
                reject(error);
            }
        });
    },
    //验证图片验证码
    confirmCode(step, phoneNumber, imageCode) {
        if (imageCode.length === 0) return ERROR.PROMISE('请输入验证码');
        let action = step === 1 ? 'sendVerify' : 'sendVerifyNew';

        return new Promise(async (resolve, reject) => {
            try {
                let result = await Req({
                    url: '/api/mine/mobile.htm',
                    type: 'POST',
                    data: {
                        action: action,
                        mobile: phoneNumber,
                        imageCode: imageCode
                    },
                    animate: true
                });

                this.message = result.errorMsg;
                resolve();
            } catch (error) {
                reject(error)
            }
        })
    },
    verifySMSCode(smsCode, type) {
        if (smsCode.length === 0) return ERROR.PROMISE('请输入验证码')

        return new Promise(async (resolve, reject) => {
            try {
                let result = await Req({
                    url: '/api/mine/mobile.htm',
                    type: 'POST',
                    data: {
                        action: 'verify',
                        type: type,
                        verifyCode: smsCode
                    },
                    animate: true
                });

                this.message = result.errorMsg;
                if (type === 2) {
                    this.getFinanceUserInfo();
                    this.getBasicUserInfo();
                }
                resolve();
            } catch (error) {
                reject(error)
            }
        })
    },
    addBankCard(bankType, province, city, cardNumber, cfmCardNumber, subbranch) {
        if (bankType.length === 0 || bankType === '请选择银行') return ERROR.PROMISE('请选择开户银行')//return AlertFunction({title:'警告', msg:'请选择开户银行'});
        if (province === '请选择开户省份') return ERROR.PROMISE('请选择开户省份')//AlertFunction({title:'警告', msg:'请选择开户省份'});
        if (city === '请选择开户城市') return ERROR.PROMISE('请选择开户城市')//AlertFunction({title:'警告', msg:'请选择开户城市'});
        if (cardNumber.length === 0 || cardNumber.length < 16) return ERROR.PROMISE('银行卡格式不正确')// AlertFunction({title:'警告', msg:'银行卡格式不正确'});
        if (cfmCardNumber.length === 0 || cfmCardNumber.length < 16) return ERROR.PROMISE('银行卡格式不正确')// AlertFunction({title:'警告', msg:'银行卡格式不正确'});
        if (!/^[\u4e00-\u9fa5_a-zA-Z]{0,}$/.test(subbranch) || subbranch.length === 0) return ERROR.PROMISE('银行卡格式不正确')//AlertFunction({title: '警告', msg: '开户支行信息格式错误，请重新输入'});
        if (!/^[^\x00-\xff]{0,}$/.test(subbranch) || subbranch.length === 0) return ERROR.PROMISE('请输入中文')
        return new Promise(async (resolve, reject) => {
            try {
                let result = await Req({
                    url: '/api/mine/bankCardAdd.htm',
                    type: 'POST',
                    data: {
                        action: 'add',
                        bank: bankType,
                        province: province,
                        city: city,
                        subbranch: subbranch,
                        cardNumber: cardNumber.replace(/\s+/g, ""),
                        cardNumberCfm: cfmCardNumber.replace(/\s+/g, ""),
                    },
                    animate: true
                });
                this.message = result.errorMsg;
                this.getFinanceUserInfo();
                this.getBasicUserInfo();

                resolve();
            } catch (error) {
                reject(error)
            }
        })
    },
    getAllBankCard() {
        return new Promise(async (resolve, reject) => {
            try {
                let result = await Req({
                    url: '/api/mine/bankCard.htm',
                    type: 'GET'
                });
                this.allBankCardList = result.bankCards;
                resolve();
            } catch (error) {
                reject(error);
            }
        })
    },
    setDefaultBankCard(id) {
        return new Promise(async (resolve, reject) => {
            try {
                let result = await Req({
                    url: '/api/mine/bankCardUpdate.htm',
                    type: 'POST',
                    data: {
                        action: 'setDefault',
                        id: id
                    },
                    animate: true
                });
                this.message = result.errorMsg;
                resolve();
            } catch (error) {
                reject(error);
            }
        })
    },
    deleteBankCard(id) {
        return new Promise(async (resolve, reject) => {
            try {
                let result = await Req({
                    url: '/api/mine/bankCardUpdate.htm',
                    type: 'POST',
                    data: {
                        action: 'del',
                        id: id
                    },
                    animate: true
                })
                this.message = result.errorMsg;
                resolve();
            } catch (error) {
                reject(error);
            }
        })
    },
    getRecord(type, beginId) {
        return new Promise(async (resolve, reject) => {
            try {

                let data = {
                    action: 'more',
                    type: type,
                }

                if (!!beginId) {
                    data.beginId = beginId
                }

                let result = await Req({
                    url: '/api/mine/funds.htm',
                    type: 'GET',
                    data: data
                });

                if (type === 1) {
                    this.transferRecordList = result.data;
                } else if (type === 2) {
                    this.transactionRecordList = result.data;
                } else if (type === 3) {
                    this.commissionRecordList = result.data;
                } else if (type === 4) {
                    this.eagleRecordList = result.data;
                }

                resolve();
            } catch (error) {
                reject(error);
            }
        });
    },
    getRecordDetail(id) {
        return new Promise(async (resolve, reject) => {
            try {
                let result = await Req({
                    url: '/api/mine/fundDetail.htm',
                    type: 'GET',
                    data: {
                        id: id
                    }
                });

                this.fundDetailData = {
                    money: result.fundDetail.money,
                    assetMoney: result.fundDetail.assetMoney,
                    type: result.fundDetail.type,
                    date: result.fundDetail.time.time,
                    desc: result.fundDetail.detail,
                    explain: result.fundDetail.explain
                }
                resolve()
            } catch (error) {
                reject(error)
            }
        })
    },
    changeBankInfo(province, city, subbranch, id) {
        return new Promise(async (resolve, reject) => {
            try {

                if (subbranch.length === 0) return ERROR.PROMISE('请输入开户支行信息');//AlertFunction({title: '警告', msg: '请输入开户支行信息'});
                if (!/^[\u4e00-\u9fa5_a-zA-Z]{0,}$/.test(subbranch)) return ERROR.PROMISE('开户支行信息输入格式错误，请重新输入');//AlertFunction({title: '警告', msg: '开户支行信息输入格式错误，请重新输入'});        

                let result = await Req({
                    url: '/api/mine/bankCardUpdate.htm',
                    type: 'POST',
                    data: {
                        action: 'update',
                        province: province,
                        city: city,
                        subbranch: subbranch,
                        id: id
                    },
                    animate: true
                })
                this.message = result.errorMsg;
                resolve();
            } catch (error) {
                reject(error);
            }
        })
    },
    nameVerification(name, idNumber) {

        if (!/^[^\x00-\xff]{0,}$/.test(name)) return ERROR.PROMISE('请输入真实姓名只支持中文');// AlertFunction({title:'警告',msg:'请输入真实姓名'});
        if (name.length === 0) return ERROR.PROMISE('请输入真实姓名');// AlertFunction({title:'警告',msg:'请输入真实姓名'});
        if (!/^[\u4e00-\u9fa5]{0,}$/.test(name)) return ERROR.PROMISE('请输入正确的真实姓名')//AlertFunction({title:'警告',msg:'请输入正确的真实姓名'});
        if (idNumber.length === 0) return ERROR.PROMISE('请输入身份证号')//AlertFunction({title:'警告',msg:'请输入身份证号'});
        if (idNumber.length !== 18) return ERROR.PROMISE('请输入正确身份证号')//AlertFunction({title:'警告',msg:'请输入正确身份证号'});

        return new Promise(async (resolve, reject) => {
            try {
                
                await Req({
                    url: '/api/mine/profileAuth.htm',
                    type: "POST",
                    data: {
                        action: 'authIdentity',
                        name: name,
                        identityNumber: idNumber
                    },
                    animate: true
                });
                this.message = '实名认证成功';
                this.getBasicUserInfo();
                this.getFinanceUserInfo();
                resolve();
            } catch (error) {
                reject(error);
            }
        });
    },
    validWithdraw() {
        return new Promise(async (resolve, reject) => {
            try {
                let result = await Req({
                    url: '/api/pay/withdraw.htm'
                });

                if (result.identityAuth === false) {
                    reject({msg: '您当前还未实名认证，为保障您的账户安全，请先实名认证', goKey: 'realNameVerification'});
                }

                if (result.bankCards && result.bankCards.length === 0) {
                    reject({msg: '您当前还未添加出金银行卡，请先添加银行卡', goKey: 'bankCardList'});
                }

                resolve();
            } catch (error) {
                reject(error);
            }
        });
    },
    withdrawMoney(money, cardId, password) {
        return new Promise(async (resolve, reject) => {
            try {

                if (password.length === 0) return ERROR.PROMISE('请输入资金密码');

                let result = await  Req({
                    url: '/api/pay/withdraw.htm',
                    type: 'POST',
                    data: {
                        type: 0,
                        action: 'apply',
                        money: money,
                        bankCard: cardId,
                        password: password
                    },
                    animate: true
                })
                this.message = result.resultMsg || result.errorMsg;
                this.getFinanceUserInfo();

                resolve();
            } catch (error) {
                reject(error);
            }
        });
    },
    /**
     * 获取推广详情
     */
    updatePromotionInfo() {
        return new Promise(async (resolve, reject) => {
            try {

                let result = await Req({
                    url: '/api/mine/union.htm',
                    type: 'GET'
                });

                let info = result.union;
                this.union.unionTotal = result.unionTotal;
                this.union.commRatio = info.commRatio * 100;
                this.union.userCount = info.userCount;
                this.union.userConsume = info.userConsume;
                this.union.visitCount = info.visitCount;
                this.union.unionVolume = result.unionVolume;
                this.union.linkAddress = `${window.location.origin}/?ru=${info.userId}`
                resolve();
            } catch (error) {
                console.log('TCL: }catch -> error', error)
                reject(error);
            }
        });
    },
    /**
     * 获取推广详情数据
     */
    updateUserList() {
        return new Promise(async (resolve, reject) => {
            try {
                let result = await Req({
                    url: '/api/mine/unionUser.htm',
                    type: 'GET',
                    animate: true
                })
                this.userList = result.users
                resolve();
            } catch (error) {
                reject(error);
            }
        });
    },
    /**
     * 找回密码第一步
     * 发送短信
     * 找回资金密码  type = 2
     */
    validCodeNum(phone, imgCode, type) {
        return new Promise(async (resolve, reject) => {
            try {
                let result = await Req({
                    url: '/api/sso/findback.htm',
                    type: 'POST',
                    data: {
                        action: 'sendCode',
                        mobile: phone,
                        imageCode: imgCode,
                        type: '' || type
                    },
                    animate: true
                })
                this.validPhone = result
                resolve();
            } catch (error) {
                reject(error);
            }
        });
    },
    /**
     * 找回密码第二步
     * 验证手机
     * 找回资金密码  type = 2
     */
    validPhoneNum(code, type) {
        return new Promise(async (resolve, reject) => {
            try {
                let result = await Req({
                    url: '/api/sso/findback.htm',
                    type: 'POST',
                    data: {
                        action: 'verifyCode',
                        verifyCode: code,
                        type: '' || type
                    },
                    animate: true
                })
                this.validPhone = result.redirectUrl
                resolve();
            } catch (error) {
                reject(error);
            }
        });
    },
    /**
     * 找回提款第三步
     * 验证身份证号
     * 找回资金密码  type = 2
     */
    validUserId(name, id, type) {
        return new Promise(async (resolve, reject) => {
            try {
                let result = await Req({
                    url: '/api/sso/findback.htm',
                    type: 'POST',
                    data: {
                        action: 'auth',
                        name: name,
                        identityNumber: id,
                        type: '' || type
                    },
                    animate: true
                })
                resolve();
            } catch (error) {
                reject(error);
            }
        });
    },
    /**
     * 找回提款第四步
     * 验证身份证号
     * 找回资金密码  type = 2
     */
    validNewPass(newPass, confirmPass, type) {
        return new Promise(async (resolve, reject) => {
            try {
                let result = await Req({
                    url: '/api/sso/findback.htm',
                    type: 'POST',
                    data: {
                        action: 'passwd',
                        newPass: newPass,
                        newPassCfm: confirmPass,
                        type: '' || type
                    },
                    animate: true
                })
                resolve();
            } catch (error) {
                reject(error);
            }
        });
    },
    /**获取签到历史接口 */
    updateCheckInStatus() {
        return new Promise(async (resolve, reject) => {
            try {
                let result = await Req({
                    url: '/api/user/checkin/history.htm',
                });

                const {pointsArray, pointsStatus} = result.data;
                const award = pointsArray.split(',');
                const status = pointsStatus.split(',');
                let count = 0;
                for (let o of status) {
                    if (o === '1') {
                        count++;
                    }
                }

                this.checkInHistory.award = award;
                this.checkInHistory.status = status;
                this.checkInHistory.count = count;
                exposure('updateCheckInStatus')
                resolve();
            } catch (error) {
                reject(error);
            }
        });
    },
    /**签到接口 */
    checkIn() {
        return new Promise(async (resolve, reject) => {
            try {
                await Req({
                    url: '/api/user/checkin/pound.htm',
                    animate: true
                });
                this.updateCheckInStatus();
                resolve();
            } catch (error) {
                reject(error);
            }
        });
    },
    /**fk里我的交易获取数据的接口 
     * ps:别问我为啥取这个名字因为我不知要怎么命名，我只是跟着旧项目的名字而已 */
    updateHeroList(){
        return new Promise(async(resolve,reject)=>{
            try {
                let result = await Req({
                    url:`/api/hero/${this.basicUserData.id}.htm`,
                    type:'GET'
                });
                
                this.heroObj.best = result.nbBetting;
                this.heroObj.historyList = result.historyList;
                this.heroObj.invest = result.invest;

                resolve();
            } catch (error) {
                reject(error);
            }
        });
    },
    /**提款记录接口 */
    updateWithdrawRecord(){
        return new Promise(async(resolve,reject)=>{
            try {
                let result = await Req({
                    url:`/api/pay/withdrawHistory.htm`,
                    type:'GET'
                });
                this.withdrawRecord = result.inouts;
                resolve();
            } catch (error) {
                reject(error);
            }
        });
    }
}