import {Req} from "../network/Req";
export default {
    /**
     *
     * @param {Number} type 0-原油 1-黄金
     * @param {Date} [date] 请求开始日期
     * @param animate
     * @returns {Promise<any>}
     */
    async updateNews(type,date,animate = false){
        return new Promise(async (resolve,reject)=>{
            try {
                const result = await Req({
                    url: '/api/news/newsList.htm',
                    type: 'GET',
                    data: {
                        type: type,
                        date: date,
                    },
                    animate: animate
                });
                this.newsList = result.newsList;
                resolve()
            } catch (error) {
                reject(error)
            }
        })
    },
    async updateLive(maxId,animate=false){
        return new Promise(async (resolve,reject)=>{
            try {
                const result = await Req({
                    url: '/api/news/expressList.htm',
                    type: 'GET',
                    data: {
                        maxId: maxId
                    },
                    animate: animate
                });

                let dataArr = [];
                for (let i = 0; i < result.newsList.length; i++) {
                    let element = result.newsList[i];
                    let spliceResult = element.split('#');
                    let obj = null;
                    if (spliceResult.length === 12) {
                        obj = {
                            origin: element,
                            date: spliceResult[2],
                            content: spliceResult[3],
                            id: spliceResult[spliceResult.length - 1]
                        }
                    } else if (spliceResult.length === 14) {
                        obj = {
                            origin: element,
                            date: spliceResult[8],
                            content: spliceResult[2],
                            id: spliceResult[spliceResult.length - 2],
                            qz: '前值：' + spliceResult[3],
                            yq: '预期：' + spliceResult[4],
                            sj: '实际：' + spliceResult[5],
                            tag: spliceResult[7],
                            star: spliceResult[6],
                            country: 'https://res.6006.com/jin10/flag/' + spliceResult[9].substr(0, 2) + '.png'
                        }
                    }
                  if(obj !== null)  dataArr.push(obj);
                }
                this.lives = dataArr;
                resolve()
            } catch (error) {
                reject(error)
            }
        })
    },
    async updateFinanceCalender(date){
        return new Promise (async(resolve,reject)=>{
            try {
                const result = await Req({
                    url: '/api/news/calendar.htm',
                    type: 'GET',
                    data: {
                        date: date
                    },
                    animate: true
                });
                this.calender = result;
                resolve();
            } catch (error) {
                reject(error);
            }
        })
    },
    async updateNotice(){
        return new Promise (async(resolve,reject)=>{
            try {
                const result = await Req({
                    url: '/api/discover/index.htm',
                    type: 'GET',
                    animate: false
                });

                for (let i = 0; i < result.notices.length; i++) {
                    const element = result.notices[i];
                    element.isOpen = false
                }

                this.notice = result.notices;
                resolve();
            } catch (error) {
                reject(error);
            }
        })
    },
    async updateCarousel(){
        return new Promise (async(resolve,reject)=>{
            try {
                if(this.homeBanner.length > 0 && this.homeNotice.length > 0) return resolve();
                const result = await Req({
                    url: '/api/index.htm',
                    type: 'GET',
                    data:{
                        action: 'carousel'
                    },
                    animate: false
                });
                this.homeNotice = result.notices;
                this.homeBanner = result.carousels;
                resolve();
            } catch (error) {
                reject(error);
            }
        })
    },
    async getNewsDetail(id){
        return new Promise (async(resolve,reject)=>{
            try {
                const result = await Req({
                    url: '/api/news/newsDetail.htm',
                    type: 'GET',
                    data:{
                        id
                    },
                    animate: true
                });

                this.newsDetail = result.news;

                resolve();
            } catch (error) {
                reject(error);
            }
        })
    },

    //新闻
    newsList : null,
    getNewList(){

        return this.newsList
    },
    //直播
    lives : null,
    getLives(){
        return this.lives
    },
    //财经日历
    calender : null,
    getCalender(){
        return this.calender;
    },
    //公告
    notice:[],
    getNotice(){
        return this.notice;
    },
    //首页公告
    homeNotice:[],
    getShortNotice(){
        return this.homeNotice;
    },
    //首页广告
    homeBanner:[],
    getBanner(){
        return this.homeBanner;
    },
    newsDetail:null,
    getNewsDetailObject(){
        return this.newsDetail;
    },
}